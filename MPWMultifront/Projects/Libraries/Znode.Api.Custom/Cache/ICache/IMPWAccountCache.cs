﻿namespace Znode.Api.Custom.Cache.ICache
{
    public interface IMPWAccountCache 
    {
        string GetUserAccountDetails(int userId, string routeUri, string routeTemplate);
    }
}
