﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Api.Custom.Cache.ICache
{
    public interface IMPWJDEIntegrationCache
    {
        string GetShippingList(bool IsActive,string routeUri, string routeTemplate);
        string GetOrderListByStateId(int OmsOrderStateId, string routeUri, string routeTemplate);
        string GetProductList(string ProductIds,string routeUri, string routeTemplate);
    }
}
