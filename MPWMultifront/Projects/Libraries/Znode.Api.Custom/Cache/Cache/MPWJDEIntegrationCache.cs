﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Api.Custom.Cache.Cache
{
    public class MPWJDEIntegrationCache : BaseCache,IMPWJDEIntegrationCache
    {
        #region Global Variable
        private readonly IShippingService _Shippingservice;
        private readonly IOrderService _orderService;
        private readonly IMPWProductService _Productservice;
        #endregion

        #region Default Constructor
        public MPWJDEIntegrationCache(IShippingService shippingService, IOrderService orderService, IMPWProductService productService)
        {
            _Shippingservice = shippingService;
            _orderService = orderService;
            _Productservice = productService;
        }
        #endregion

        #region Public Methods
        public virtual string GetOrderListByStateId(int OmsOrderStateId,string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Get data from service
                OrdersListModel orderList = _orderService.GetOrderList(Expands, Filters, Sorts, Page);
                if (orderList?.Orders?.Count > 0 || IsNotNull(orderList?.CustomerName))
                {
                    List<OrderModel> ordersIds = orderList.Orders.Where(x => x.OmsOrderStateId == OmsOrderStateId).ToList<OrderModel>();
                    orderList.Orders = new List<OrderModel> ();
                    foreach (OrderModel order in ordersIds)
                    {
                        OrderService orderService = new OrderService();
                        OrderModel orderModel = orderService.GetOrderById(order.OmsOrderId, Filters, Expands);
                        orderList.Orders.Add(orderModel);

                    }
                    OrderListResponse response = new OrderListResponse { OrderList = orderList };

                    response.MapPagingDataFromModel(orderList);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        //returns the dataset consisting of Order,OrderDetail,ZnodeShipping,OrderLineItems,ZnodeOmsOrderState.
        protected virtual DataSet GetDataSetByOrderId(int orderId)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();

            objStoredProc.GetParameter("@OmsOrderId", orderId, ParameterDirection.Input, SqlDbType.Int);
            return objStoredProc.GetSPResultInDataSet("Znode_GetOrderDetailsByOrderId");
        }
        //returns the dataset consisting of Order,OrderDetail,ZnodeShipping,OrderLineItems,ZnodeOmsOrderState.

        //Get shipping list.
        public virtual string GetShippingList(bool IsActive, string routeUri, string routeTemplate)
        {
            //Get data from cache.
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //shipping list
                ShippingListModel shippingList = _Shippingservice.GetShippingList(Expands, Filters, Sorts, Page);
                if (shippingList?.ShippingList?.Count > 0)
                {
                    shippingList.ShippingList = shippingList.ShippingList.Where(x => x.IsActive == IsActive).ToList<ShippingModel>();
                    //Get response and insert it into cache.
                    ShippingListResponse response = new ShippingListResponse { ShippingList = shippingList.ShippingList };
                    response.MapPagingDataFromModel(shippingList);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

       // Get Products From Cache
        public virtual string GetProductList(string ProductIds, string routeUri, string routeTemplate)
        {
            // FilterCollection filters = Filters;
            string data = GetFromCache(routeUri);
            FilterCollection filters = new FilterCollection();
            filters.Add("ProductId", FilterOperators.In, ProductIds);
            //If Data From Cache Is Null Then Give Service call
            if (string.IsNullOrEmpty(data))
            {
                ProductDetailsListModel productList = _Productservice.GetProductList(Expands, filters, Sorts, Page);
                
                //Generate Response
                ProductListResponse response = GetProductListResponseFromDetailListModel(productList);

                data = InsertIntoCache(routeUri, routeTemplate, response);
            }
            return data;
        }

        //Generate response
        public ProductListResponse GetProductListResponseFromDetailListModel(ProductDetailsListModel productList)
        {
            ProductListResponse response = new ProductListResponse
            {
                ProductDetails = productList.ProductDetailList,
                Locale = productList.Locale,
                AttrubuteColumnName = productList.AttributeColumnName,
                XmlDataList = productList.XmlDataList,
                ProductDetailsDynamic = productList?.ProductDetailListDynamic,
                NewAttributeList = productList?.NewAttributeList
            };
            response.MapPagingDataFromModel(productList);
            return response;
        }

        #endregion
    }
}
