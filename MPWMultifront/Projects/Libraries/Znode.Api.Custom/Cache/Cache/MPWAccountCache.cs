﻿using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Custom.Service.Service;
using Znode.Api.Model.Custom.MPWAccountModel;
using Znode.Api.Model.Custom.MPWUserAccountListResponse;
using Znode.Engine.Api.Cache;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Cache.Cache
{
    public class MPWAccountCache : BaseCache, IMPWAccountCache
    {
        #region private variables
        private readonly IMPWAccountService _mpwAccountService;
        #endregion

        #region public constructor
        public MPWAccountCache()
        {
            _mpwAccountService = new MPWAccountService();
        }
        #endregion

        #region public methods
        /// <summary>
        /// Get User Account Details
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        public virtual string GetUserAccountDetails(int userId, string routeUri, string routeTemplate)
        {
            string str = this.GetFromCache(routeUri);
            if (string.IsNullOrEmpty(str))
            {
                MPWUserAccountListModel data = _mpwAccountService.GetUserAccountDetails(userId);
                if (HelperUtility.IsNotNull((object)data))
                {
                    MPWUserAccountListResponse Response = new MPWUserAccountListResponse()
                    {
                        UserAccountDetails = data
                    };
                    str = this.InsertIntoCache(routeUri, routeTemplate, (object)Response);
                }
            }
            return str;
        }
        #endregion
    }
}
