﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Znode.Api.Custom.Cache.Cache;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Model.Custom.MPWUserAccountListResponse;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class MPWAccountController : BaseController
    {
        #region private variables
        private readonly IMPWAccountCache _mpwAccountCache;
        #endregion

        #region public constructor
        public MPWAccountController()
        {
            _mpwAccountCache = new MPWAccountCache();
        }
        #endregion

        #region public methods
        /// <summary>
        /// Get User Account Details
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [ResponseType(typeof(MPWUserAccountListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetUserAccountDetails(int userId)
        {
            HttpResponseMessage httpResponseMessage;
            try
            {
                string data = _mpwAccountCache.GetUserAccountDetails(userId, RouteUri, RouteTemplate);
                httpResponseMessage = !string.IsNullOrEmpty(data) ? CreateOKResponse<MPWUserAccountListResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage((Exception)ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Warning, (object)null);
                MPWUserAccountListResponse data = new MPWUserAccountListResponse();
                data.HasError = true;
                data.ErrorMessage = ((Exception)ex).Message;
                data.ErrorCode = ex.ErrorCode;
                httpResponseMessage = CreateInternalServerErrorResponse<MPWUserAccountListResponse>(data);
            }
            catch (Exception ex)
            {
                MPWUserAccountListResponse data = new MPWUserAccountListResponse();
                data.HasError = true;
                data.ErrorMessage = ex.Message;
                httpResponseMessage = CreateInternalServerErrorResponse<MPWUserAccountListResponse>(data);
                ZnodeLogging.LogMessage(ex, ((ZnodeLogging.Components)2).ToString(), TraceLevel.Error, (object)null);
            }
            return httpResponseMessage;
        }
        #endregion
    }
}
