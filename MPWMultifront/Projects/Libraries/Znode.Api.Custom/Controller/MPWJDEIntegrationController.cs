﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Cache.Cache;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class MPWJDEIntegrationController : BaseController
    {
        #region Private Variables
        private readonly IMPWJDEIntegrationCache _cache;
        private readonly IShippingService _Shippingservice; 
        private readonly IOrderService _orderService;

       // private readonly IProductCache _Productcache;
        private readonly IMPWProductService _Productservice;
        #endregion
        #region Constructor
        public MPWJDEIntegrationController(IShippingService Shippingservice, IOrderService orderService, IMPWProductService Productservice)
        {
            _Shippingservice = Shippingservice;
            _orderService = orderService;
            _Productservice = Productservice;
            _cache = new MPWJDEIntegrationCache(_Shippingservice, _orderService, _Productservice);
           

        }
        #endregion
        /// <summary>
        /// Gets list of shipping.
        /// </summary>
        /// <returns>Returns shipping list.</returns>
        [ResponseType(typeof(ShippingListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage ShippingList(bool IsActive)
        {
            HttpResponseMessage response;
            try
            {
                //Get Shippings.
                string data = _cache.GetShippingList(IsActive, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<ShippingListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ProviderEngine.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new ShippingListResponse { HasError = true, ErrorMessage = ex.Message });
            }

            return response;
        }

        /// <summary>
        /// Get the list of all Orders.
        /// </summary>
        /// <returns>Returns list of all orders.</returns>
        [ResponseType(typeof(OrderListResponse))]
        [HttpGet]
        public HttpResponseMessage OrderListByStateid(int OmsOrderStateId)
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetOrderListByStateId(OmsOrderStateId,RouteUri, RouteTemplate);
                response = string.IsNullOrEmpty(data) ? CreateNoContentResponse() : CreateOKResponse<OrderListResponse>(data);
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new OrderListResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new OrderListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;
        }
        /// <summary>
        /// Update order status.
        /// </summary>
        /// <param name="model">OrderStateParameterModel</param>
        /// <returns>Returns true if updated sucessfully else return false.</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        public HttpResponseMessage UpdateOrderStatus([FromBody] OrderStateParameterModel model)
        {
            HttpResponseMessage response;
            try
            {
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _orderService.UpdateOrderStatus(model) });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return response;
        }
        /// <summary>
        /// Get List of Product.
        /// </summary>
        /// <returns>Return list of product.</returns>
        [ResponseType(typeof(ProductListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetProductList(string ProductIds)
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetProductList(ProductIds,RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<ProductListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                ProductListResponse data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
    }
}
