﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Dynamic;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Model.Custom.MPWAccountModel;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.Service.Service
{
    public class MPWAccountService : AccountService, IMPWAccountService
    {
        #region Private Variables
        protected readonly IZnodeRepository<ZnodeGlobalAttribute> _globalAttributeRepository;
        protected readonly IZnodeRepository<ZnodeGlobalAttributeDefaultValue> _defaultValueRepository;
        protected readonly IZnodeRepository<ZnodeGlobalAttributeDefaultValueLocale> _defaultValueLocaleRepository;
        protected readonly IUserService _userService;
        #endregion

        #region Constructor
        public MPWAccountService() : base()
        {
            _globalAttributeRepository = new ZnodeRepository<ZnodeGlobalAttribute>();
            _defaultValueRepository = new ZnodeRepository<ZnodeGlobalAttributeDefaultValue>();
            _defaultValueLocaleRepository = new ZnodeRepository<ZnodeGlobalAttributeDefaultValueLocale>();
            _userService = GetService<IUserService>();
        }
        #endregion
        public override AccountDataModel CreateAccount(AccountDataModel model)
        {
            AccountDataModel accountmodel = base.CreateAccount(model);
            if (accountmodel != null)
            {
                List<GlobalAttributeDefaultValueLocaleModel> list = new List<GlobalAttributeDefaultValueLocaleModel>();
                string attributecode = Convert.ToString(ConfigurationManager.AppSettings["AdditionalAccountAttributeId"]);
                int? attributeid = _globalAttributeRepository.Table.Where(x => x.AttributeCode == attributecode).First()?.GlobalAttributeId;

                GlobalAttributeService global = new GlobalAttributeService();
                GlobalAttributeDefaultValueModel globalAttribute = new GlobalAttributeDefaultValueModel();
                GlobalAttributeDefaultValueLocaleModel value = new GlobalAttributeDefaultValueLocaleModel();
                value.LocaleId = 1;
                value.DefaultAttributeValue = accountmodel.CompanyAccount.Name;
                list.Add(value);
                globalAttribute.AttributeDefaultValueCode = accountmodel.CompanyAccount.AccountCode;
                globalAttribute.ValueLocales = list;
                globalAttribute.IsDefault = false;
                globalAttribute.IsSwatch = false;
                globalAttribute.SwatchText = "#FFFFFF";
                globalAttribute.DisplayOrder = 1;
                globalAttribute.GlobalAttributeId = attributeid;
                global.SaveDefaultValues(globalAttribute);
            }
            return accountmodel;
        }

        public override bool Update(AccountDataModel model)
        {
            bool status = base.Update(model);
            if (status)
            {
                List<GlobalAttributeDefaultValueLocaleModel> list = new List<GlobalAttributeDefaultValueLocaleModel>();
                string attributecode = Convert.ToString(ConfigurationManager.AppSettings["AdditionalAccountAttributeId"]);
                int? attributeid = _globalAttributeRepository.Table.Where(x => x.AttributeCode == attributecode).First()?.GlobalAttributeId;

                int? defaultValueId = _defaultValueRepository.Table.Where(x => x.AttributeDefaultValueCode == model.CompanyAccount.AccountCode && x.GlobalAttributeId == attributeid)?.First().GlobalAttributeDefaultValueId;
                if (defaultValueId != null || defaultValueId > 0)
                {
                    try
                    {
                        GlobalAttributeDefaultValueLocaleModel item = new GlobalAttributeDefaultValueLocaleModel();
                        item.GlobalDefaultAttributeValueId = defaultValueId;
                        item.DefaultAttributeValue = model.CompanyAccount.Name;
                        item.LocaleId = GetDefaultLocaleId();
                        FilterCollection filterCol = new FilterCollection();
                        filterCol.Add(new FilterTuple(ZnodeGlobalAttributeLocaleEnum.LocaleId.ToString(), ProcedureFilterOperators.Equals, item.LocaleId.ToString()));
                        filterCol.Add(new FilterTuple(ZnodeGlobalAttributeDefaultValueEnum.GlobalAttributeDefaultValueId.ToString(), ProcedureFilterOperators.Equals, item.GlobalDefaultAttributeValueId.ToString()));

                        ZnodeGlobalAttributeDefaultValueLocale _localeEntity = _defaultValueLocaleRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filterCol.ToFilterDataCollection()).WhereClause);
                        ZnodeGlobalAttributeDefaultValueLocale defaultValueLocaleEntity = GlobalAttributeMap.ToDefaultValueLocaleEntity(item);
                        defaultValueLocaleEntity.GlobalAttributeDefaultValueLocaleId = _localeEntity.GlobalAttributeDefaultValueLocaleId;
                        _defaultValueLocaleRepository.Update(defaultValueLocaleEntity);

                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }

                }
            }
            return status;
        }

        /// <summary>
        /// Get User Account Details
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public virtual MPWUserAccountListModel GetUserAccountDetails(int userId)
        {
            UserModel userInfo = _userService.GetUserById(userId, null);
            List<MPWAccountModel> list = GetAccountDetails(userId);

            MPWUserAccountListModel userAccountListModel = new MPWUserAccountListModel()
            {
                AccountDetails = list,
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName,
                Email = userInfo.Email,
                PhoneNumber = userInfo.PhoneNumber,
                UserId = Convert.ToString(userInfo.UserId),
                UserName = userInfo.UserName,
                PortalId = userInfo.PortalId
            };
            return userAccountListModel;
        }

        /// <summary>
        /// get user account details
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        protected virtual List<MPWAccountModel> GetAccountDetails(int userId)
        {
            try
            {
                if (userId > 0)
                {
                    IZnodeViewRepository<MPWAccountModel> objStoredProc = new ZnodeViewRepository<MPWAccountModel>();
                    objStoredProc.SetParameter("@userId", userId, ParameterDirection.Input, DbType.Int32);
                    List<MPWAccountModel> account = objStoredProc.ExecuteStoredProcedureList("ZnodeMpwGetAccountDetailsByUserId @userId").ToList();
                    return account;
                }
                return new List<MPWAccountModel>();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return new List<MPWAccountModel>();
            }
        }
    }
}
