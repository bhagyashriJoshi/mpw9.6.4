﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service.Service
{
    public class MPWProductService: BaseService, IMPWProductService
    {
        public virtual ProductDetailsListModel GetProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            //Sets to true when need to exclude some products from list.
            bool isNotInFilter = false;

            string productIdsToExclude = "";
            ZnodeLogging.LogMessage("productIdsToExclude returned from GetProductIdsToExculde:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, productIdsToExclude);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel for GetXmlProduct:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            if (productIdsToExclude == string.Empty)
            {
                productIdsToExclude = filters.Where(x => x.Item1.Equals("ProductId", StringComparison.InvariantCultureIgnoreCase))?.Select(x => x.FilterValue)?.FirstOrDefault();
            }
            ProductService productService = new ProductService();
            var xmlList = productService.GetXmlProduct(filters, pageListModel, productIdsToExclude, isNotInFilter);

            ProductDetailsListModel productList = new ProductDetailsListModel
            {
                Locale = GetActiveLocaleList(),
                AttributeColumnName = xmlList.AttributeColumnName,
                XmlDataList = xmlList.XmlDataList
            };
            productList.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return productList;
        }

      
    }
}
