﻿using Znode.Api.Model.Custom.MPWAccountModel;

namespace Znode.Api.Custom.Service.IService
{
    public interface IMPWAccountService
    {
        MPWUserAccountListModel GetUserAccountDetails(int userId);
    }
}
