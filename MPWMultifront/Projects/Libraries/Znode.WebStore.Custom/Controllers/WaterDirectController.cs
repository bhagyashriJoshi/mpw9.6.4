﻿using Microsoft.AspNet.Identity;
using System;
using System.Web.Http;
using System.Web.Mvc;
using Znode.MPW.Service.DataModels;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;
using Znode.WebStore.Custom.ViewModel.Enums;

namespace Znode.WebStore.Custom.Controllers
{
    public class WaterDirectController : WdBaseController
    {
        private readonly IWaterDirectData _waterDirectData;

        private int _userId { get; set; }

        public WaterDirectController(IWaterDirectData waterDirectData)
        {
            try
            {
                _waterDirectData = waterDirectData;
                //default for testing
                var userAccountId = 0;
                if (User != null)
                {
                    var userId = User?.Identity?.GetUserId();
                    userAccountId = int.Parse((string.IsNullOrEmpty(userId) ? "9" : userId));
                }
                else
                {
                    userAccountId = 9;
                }
                _userId = userAccountId;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public ActionResult Index()
        {
            try
            {
                string account = Request.QueryString.Get("account");
                ViewBag.Title = "MPW Water Direct";
                var model = _waterDirectData.GetLandingPageData(_userId, account);
                return ActionView("_WdLanding", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #region Mobile Equipment

        //[Route("mobile-equipment")]
        public ActionResult MobileEquipment()
        {
            try
            {
                ViewBag.Title = "MPW Water Direct - Mobile Equipment";
                var model = _waterDirectData.GetMobileEquipmentData(_userId);
                //check if user has access, if not, also redirect to home page
                var isEnabled = model.WaterDirectNavigation.WaterDirectNavPermissions.CanViewMobileEquipment;
                if (!IsAuthenticated(isEnabled)) return LandingPageView();
                return ActionView("_MobileEquipment", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #endregion Mobile Equipment

        #region DIEquipment
        
        public ActionResult DiEquipment(string account, int pageSize = 20, int pageIndex = 0, DateTime? start = null, DateTime? end = null)
        {
            try
            {
                var model = _waterDirectData.GetDiEquipmentData(account, pageSize, pageIndex, start, end, true);
                //check if user has access, if not, also redirect to home page
                var isEnabled = model.WaterDirectNavigation.WaterDirectNavPermissions.CanViewDiEquipment;
                if (!IsAuthenticated(isEnabled)) return LandingPageView();
                ViewBag.Title = "MPW Water Direct - DI Equipment";
                return ActionView("_DiEquipment", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult GetDiOrderHistory([FromBody] DiEquipmentDto model)
        {
            try
            {
                string account = model.WaterDirectNavigation.SelectedUserAssignedAccount;
                int pageSize = model.PageSize;
                int pageIndex = model.PageIndex;
                DateTime? startDate = model.StartDate;
                DateTime? endDate = model.EndDate;
                bool refreshCache = model.RefreshCache;
                
                var responseModel = _waterDirectData.GetDiEquipmentData(account, pageSize, pageIndex, startDate, endDate, refreshCache);
                return PartialView("_DiOrderHistory", responseModel);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("WaterDirect/DiEquipment/ViewWorkOrder")]
        public ActionResult ViewWorkOrder(string account, Guid workOrderId, string backUrl)
        {
            try
            {
                if (!User.Identity.IsAuthenticated) return RedirectToAction("Index", "WaterDirect");
                ViewBag.Title = "MPW Water Direct - DI Equipment - Work Order";
                var model = _waterDirectData.GetDiWorkOrderData(account, workOrderId, backUrl);
                return ActionView("_DiViewWorkOrder", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("WaterDirect/DiEquipment/Order")]
        public ActionResult Order(string account, string backUrl)
        {
            try
            {
                if (!User.Identity.IsAuthenticated) return RedirectToAction("Index", "WaterDirect");
                ViewBag.Title = "MPW Water Direct - DI Equipment - Order";

                var model = _waterDirectData.GetDiOrderData(account, backUrl);
                
                return ActionView("_DiOrderTrailer", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Route("WaterDirect/DiEquipment/Submit")]
        public ActionResult Submit(DiOrderDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string account = model.WaterDirectNavigation.SelectedUserAssignedAccount;
                    bool isSwap = model.IsSwap;
                    string deliveryOption = model.DeliveryOption;
                    DateTime? deliveryDate = DateTime.Parse(model.DeliveryDate);
                    string PONumber = model.PONumber;
                    Guid selectedDILoad = model.SelectedDILoad.LoadID;
                    int trailerQuantity = model.TrailerQuantity;
                    string orderNotes = model.OrderNotes;

                    int confirmationNumber = _waterDirectData.SubmitDiOrder(_userId, account, isSwap, deliveryOption, deliveryDate, PONumber, selectedDILoad, trailerQuantity, orderNotes);
                    if (confirmationNumber > 0)
                    {
                        return Json(model);
                    }
                    else
                    {
                        return Json("Order failed to submit");
                    }
                }
                else
                {
                    return Json("Model is not valid");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #endregion DI Equipment

        #region Boom Equipment

        //[Route("boom-equipment")]
        public ActionResult BoomEquipment()
        {
            try
            {
                var model = _waterDirectData.GetBoomEquipmentData(_userId);
                //check if user has access, if not, also redirect to home page
                var isEnabled = model.WaterDirectNavigation.WaterDirectNavPermissions.CanViewBoomEquipment;
                if (!IsAuthenticated(isEnabled)) return LandingPageView();
                ViewBag.Title = "MPW Water Direct - Boom Equipment";
                return ActionView("_BoomEquipment", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("BoomEquipment/BoomRefresh")]
        public string BoomRefresh()
        {
            try
            {
                //do something
                return "";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("BoomEquipment/BoomFileDownloads")]
        public void BoomFileDownloads()
        {
            try
            {
                //get boom file downloads
                //load in modal
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Route("BoomEquipment/BuildTrend")]
        public void BuildTrend()
        {
            try
            {
                //do something
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [System.Web.Mvc.HttpPost]
        [System.Web.Mvc.Route("BoomEquipment/BuildSummary")]
        public void BuildSummary(DateTime startDate, DateTime endDate)
        {
            try
            {
                //do something
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #endregion Boom Equipment
    }
}