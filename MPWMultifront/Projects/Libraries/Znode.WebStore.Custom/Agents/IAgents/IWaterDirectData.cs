﻿using System;
using Znode.WebStore.Custom.ViewModel;
using Znode.WebStore.Custom.ViewModel.Enums;
using BoomEquipmentDto = Znode.WebStore.Custom.ViewModel.BoomEquipmentDto;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IWaterDirectData
    {
        BoomEquipmentDto GetBoomEquipmentData(int userId);

        DiEquipmentDto GetDiEquipmentData(string account, int size, int index, DateTime? start, DateTime? end,
            bool refreshCache = false);

        DiWorkOrderDto GetDiWorkOrderData(string account, Guid workOrderId, string backUrl);
        DiOrderDto GetDiOrderData(string account, string backUrl);

        int SubmitDiOrder(int userId, string accountId, bool isSwap, string deliverOption, DateTime? deliveryDate,
            string PONumber, Guid selectedDILoadId, int trailerQuantity, string orderNotes);

        MobileEquipmentDto GetMobileEquipmentData(int userId);
        WdLandingDto GetLandingPageData(int userId, string accountId);
        //WaterDirectUserAccountListDto GetUserAccountList(int userId);
    }
}