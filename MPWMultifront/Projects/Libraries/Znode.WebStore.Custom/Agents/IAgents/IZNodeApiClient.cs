﻿namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IZNodeApiClient
    {
        string GetUserAccountDetails(int userId);
    }
}
