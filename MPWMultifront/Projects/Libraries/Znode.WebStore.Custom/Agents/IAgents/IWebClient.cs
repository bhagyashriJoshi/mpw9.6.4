﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IWebClient : IDisposable
    {
        bool AllowReadStreamBuffering { get; set; }

        bool AllowWriteStreamBuffering { get; set; }

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        string BaseAddress { get; set; }

        RequestCachePolicy CachePolicy { get; set; }

        IContainer Container { get; }

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        ICredentials Credentials { get; set; }

        /// <devdoc>
        ///    <para>Sets the encoding type for converting string to byte[] on String based methods</para>
        /// </devdoc>
        Encoding Encoding { get; set; }

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        WebHeaderCollection Headers { get; set; }

        /// <devdoc>
        ///    <para>
        ///       Indicates if the request is still in progress
        ///    </para>
        /// </devdoc>
        bool IsBusy { get; }

        /// <devdoc>
        ///    <para>
        ///       Gets or sets the proxy information for a request.
        ///    </para>
        /// </devdoc>
        IWebProxy Proxy { get; set; }

        NameValueCollection QueryString { get; set; }

        WebHeaderCollection ResponseHeaders { get; }

        ISite Site { get; set; }

        /// <devdoc>
        ///    <para>Sets Credentials to CredentialCache.DefaultCredentials</para>
        /// </devdoc>
        bool UseDefaultCredentials { get; set; }

        void CancelAsync();

        //ObjRef CreateObjRef(Type requestedType);

        //void Dispose();

        event EventHandler Disposed;

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        byte[] DownloadData(string address);

        byte[] DownloadData(Uri address);

        void DownloadDataAsync(Uri address);

        void DownloadDataAsync(Uri address, object userToken);

        event DownloadDataCompletedEventHandler DownloadDataCompleted;

        Task<byte[]> DownloadDataTaskAsync(string address);

        Task<byte[]> DownloadDataTaskAsync(Uri address);

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        void DownloadFile(string address, string fileName);

        void DownloadFile(Uri address, string fileName);

        void DownloadFileAsync(Uri address, string fileName);

        void DownloadFileAsync(Uri address, string fileName, object userToken);

        event AsyncCompletedEventHandler DownloadFileCompleted;

        Task DownloadFileTaskAsync(string address, string fileName);

        Task DownloadFileTaskAsync(Uri address, string fileName);

        event DownloadProgressChangedEventHandler DownloadProgressChanged;

        /// <devdoc>
        ///    <para>Downloads a string from the server</para>
        /// </devdoc>
        string DownloadString(string address);

        string DownloadString(Uri address);

        void DownloadStringAsync(Uri address);

        void DownloadStringAsync(Uri address, object userToken);

        event DownloadStringCompletedEventHandler DownloadStringCompleted;

        Task<string> DownloadStringTaskAsync(string address);

        Task<string> DownloadStringTaskAsync(Uri address);

        object GetLifetimeService();

        object InitializeLifetimeService();

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        Stream OpenRead(string address);

        Stream OpenRead(Uri address);

        void OpenReadAsync(Uri address);

        void OpenReadAsync(Uri address, object userToken);

        event OpenReadCompletedEventHandler OpenReadCompleted;

        Task<Stream> OpenReadTaskAsync(string address);

        Task<Stream> OpenReadTaskAsync(Uri address);

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        Stream OpenWrite(string address);

        Stream OpenWrite(Uri address);

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        Stream OpenWrite(string address, string method);

        Stream OpenWrite(Uri address, string method);

        void OpenWriteAsync(Uri address);

        void OpenWriteAsync(Uri address, string method);

        void OpenWriteAsync(Uri address, string method, object userToken);

        event OpenWriteCompletedEventHandler OpenWriteCompleted;

        Task<Stream> OpenWriteTaskAsync(string address);

        Task<Stream> OpenWriteTaskAsync(Uri address);

        Task<Stream> OpenWriteTaskAsync(string address, string method);

        Task<Stream> OpenWriteTaskAsync(Uri address, string method);

        string ToString();

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        byte[] UploadData(string address, byte[] data);

        byte[] UploadData(Uri address, byte[] data);

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        byte[] UploadData(string address, string method, byte[] data);

        byte[] UploadData(Uri address, string method, byte[] data);

        void UploadDataAsync(Uri address, byte[] data);

        void UploadDataAsync(Uri address, string method, byte[] data);

        void UploadDataAsync(Uri address, string method, byte[] data, object userToken);

        event UploadDataCompletedEventHandler UploadDataCompleted;

        Task<byte[]> UploadDataTaskAsync(string address, byte[] data);

        Task<byte[]> UploadDataTaskAsync(Uri address, byte[] data);

        Task<byte[]> UploadDataTaskAsync(string address, string method, byte[] data);

        Task<byte[]> UploadDataTaskAsync(Uri address, string method, byte[] data);

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        byte[] UploadFile(string address, string fileName);

        byte[] UploadFile(Uri address, string fileName);

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        byte[] UploadFile(string address, string method, string fileName);

        byte[] UploadFile(Uri address, string method, string fileName);

        void UploadFileAsync(Uri address, string fileName);

        void UploadFileAsync(Uri address, string method, string fileName);

        void UploadFileAsync(Uri address, string method, string fileName, object userToken);

        event UploadFileCompletedEventHandler UploadFileCompleted;

        Task<byte[]> UploadFileTaskAsync(string address, string fileName);

        Task<byte[]> UploadFileTaskAsync(Uri address, string fileName);

        Task<byte[]> UploadFileTaskAsync(string address, string method, string fileName);

        Task<byte[]> UploadFileTaskAsync(Uri address, string method, string fileName);

        event UploadProgressChangedEventHandler UploadProgressChanged;

        /// <devdoc>
        ///    <para>Uploads a string of data and returns a string of data</para>
        /// </devdoc>
        string UploadString(string address, string data);

        string UploadString(Uri address, string data);

        /// <devdoc>
        ///    <para>Uploads a string of data and returns a string of data</para>
        /// </devdoc>
        string UploadString(string address, string method, string data);

        string UploadString(Uri address, string method, string data);

        void UploadStringAsync(Uri address, string data);

        void UploadStringAsync(Uri address, string method, string data);

        void UploadStringAsync(Uri address, string method, string data, object userToken);

        event UploadStringCompletedEventHandler UploadStringCompleted;

        Task<string> UploadStringTaskAsync(string address, string data);

        Task<string> UploadStringTaskAsync(Uri address, string data);

        Task<string> UploadStringTaskAsync(string address, string method, string data);

        Task<string> UploadStringTaskAsync(Uri address, string method, string data);

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        byte[] UploadValues(string address, NameValueCollection data);

        byte[] UploadValues(Uri address, NameValueCollection data);

        /// <devdoc>
        ///    <para>[To be supplied.]</para>
        /// </devdoc>
        byte[] UploadValues(string address, string method, NameValueCollection data);

        byte[] UploadValues(Uri address, string method, NameValueCollection data);

        void UploadValuesAsync(Uri address, NameValueCollection data);

        void UploadValuesAsync(Uri address, string method, NameValueCollection data);

        void UploadValuesAsync(Uri address, string method, NameValueCollection data, object userToken);

        event UploadValuesCompletedEventHandler UploadValuesCompleted;

        Task<byte[]> UploadValuesTaskAsync(string address, NameValueCollection data);

        Task<byte[]> UploadValuesTaskAsync(string address, string method, NameValueCollection data);

        Task<byte[]> UploadValuesTaskAsync(Uri address, NameValueCollection data);

        Task<byte[]> UploadValuesTaskAsync(Uri address, string method, NameValueCollection data);

        event WriteStreamClosedEventHandler WriteStreamClosed;
    }
}

