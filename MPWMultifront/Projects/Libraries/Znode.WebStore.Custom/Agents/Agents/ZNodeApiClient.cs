﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI.WebControls;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.Helper;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class ZNodeApiClient : IZNodeApiClient
    {
        //private readonly IConfiguration _config;
        //private readonly ILog _logger;
        private string ApiHost { get; set; }
        private string OldApiHost { get; set; }
        private readonly IWebClient _webClient;

        public ZNodeApiClient()//(ILog logger)
        {
            //_logger = logger;
            ApiHost = ConfigHelper.ApiHost;
            OldApiHost = ConfigHelper.OldApiHost;
            var client = new SystemWebClient
            {
                Encoding = Encoding.UTF8,
                UseDefaultCredentials = true
            };
            client.Headers[HttpRequestHeader.Authorization] = GetAuthorizationString();
            _webClient = client;
        }

        public string GetUserAccountDetails(int userId)
        {
            string url = $"https://{ApiHost}/mpwaccount/getuseraccountdetails/{userId}";
            _webClient.Headers[HttpRequestHeader.Authorization] = GetAuthorizationString();
            var data = _webClient.DownloadString(url);
            return data;
        }

        private string GetAuthorizationString()
        {
            string pw = ConfigHelper.ApiPw;
            byte[] authorizationBytes = Encoding.UTF8.GetBytes($"{ApiHost}|{pw}");
            return $"Basic {Convert.ToBase64String(authorizationBytes)}";
        }

        private string PostData(byte[] data, string address)
        {
            string retval = string.Empty;
            _webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
            _webClient.Headers[HttpRequestHeader.Accept] = "application/json";
            try
            {
                byte[] returnVal = _webClient.UploadData(address, "POST", data);
                retval = Encoding.UTF8.GetString(returnVal);
            }
            catch (Exception ex)
            {
            }
            return retval;
        }

        private string PostString(string data, string address)
        {
            string retval = string.Empty;

            _webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
            _webClient.Headers[HttpRequestHeader.Accept] = "application/json";
            try
            {
                 retval = _webClient.UploadString(address, "POST", data);
            }
            catch (Exception ex)
            {
            }
            return retval;
        }

        private string PutData(byte[] data, string address)
        {
            string retval = string.Empty;
            _webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
            _webClient.Headers[HttpRequestHeader.Accept] = "application/json";
            try
            {
                retval = Encoding.UTF8.GetString(_webClient.UploadData(address, "PUT", data));
            }
            catch (Exception ex)
            {
            }
            return retval;
        }
    }
}
