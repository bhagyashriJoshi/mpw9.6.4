﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.MPW.Service;
using Znode.MPW.Service.DataModels;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.Helper;
using Znode.WebStore.Custom.ViewModel;
using BoomEquipmentDto = Znode.WebStore.Custom.ViewModel.BoomEquipmentDto;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class WaterDirectData : IWaterDirectData
    {
        private readonly IMPWWebService _mpwWebService;
        private readonly IZNodeApiClient _zNodeApiClient;

        public WaterDirectData(IMPWWebService mpwWebService, IZNodeApiClient zNodeApiClient)
        {
            try
            {
                _mpwWebService = mpwWebService;
                _zNodeApiClient = zNodeApiClient;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #region Landing page

        public WdLandingDto GetLandingPageData(int userId, string accountId)
        {
            try
            {
                //TODO: determine what nav buttons are available to the user
                var data = new WdLandingDto
                {
                    WaterDirectNavigation = WaterDirectNavigationHelper.DefaultPermissions
                };

                //get user account list from znode
                WaterDirectUserAccountListDto userAccountList = GetUserAccountZNodeDetails(userId);
                //pull jdenumbers from znode data
                List<string> jdeNumbers = GetUserAssociatedCustomerJdeNumbers(userAccountList);
                //get wmc customer accounts using znode jde numbers
                List<CustomerDetailsDTO> wmcFacilities = GetUserAssociatedCustomerAccounts(jdeNumbers);

                //set navigation
                data.WaterDirectNavigation = WaterDirectNavigationHelper.UserPermissions(userAccountList, accountId);
                //populate dropdown list

                data.WaterDirectNavigation.WaterDirectNavPermissions.CanViewMobileEquipment = false;
                data.WaterDirectNavigation.UserAssignedAccountsSelectList = GetUserAssignedAccountsSelectList(wmcFacilities);
                data.WaterDirectNavigation.SelectedUserAssignedAccount = accountId;
                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        #endregion Landing page

        #region Boom Equipment

        public BoomEquipmentDto GetBoomEquipmentData(int userId)
    {
        try
        {
            var data = new BoomEquipmentDto
            {
                BoomEquipmentListTypes = GetBoomEquipmentListTypes(),
                BoomFileDownloads = GetBoomFileDownloads(),
                WaterDirectNavigation = WaterDirectNavigationHelper.DefaultPermissions
            };
            //get user account list from znode
            WaterDirectUserAccountListDto userAccountList = GetUserAccountZNodeDetails(userId);
            //pull jdenumbers from znode data
            List<string> jdeNumbers = GetUserAssociatedCustomerJdeNumbers(userAccountList);
            //get wmc customer accounts using znode jde numbers
            List<CustomerDetailsDTO> wmcFacilities = GetUserAssociatedCustomerAccounts(jdeNumbers);

            data.WaterDirectNavigation.UserAssignedAccountsSelectList = GetUserAssignedAccountsSelectList(wmcFacilities);
            return data;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

        private List<BoomFileDownloadDto> GetBoomFileDownloads()
        {
            try
            {
                var files = new List<BoomFileDownloadDto>();
                files.AddRange(new[]
                {
                    new BoomFileDownloadDto() { FileName = "FIle 3", FilePath = @"\\mpwservices\files\1.pdf" },
                    new BoomFileDownloadDto() { FileName = "FIle 3", FilePath = @"\\mpwservices\files\2.pdf" },
                    new BoomFileDownloadDto() { FileName = "FIle 3", FilePath = @"\\mpwservices\files\3.pdf" }
                });
                return files;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private SelectList GetBoomEquipmentListTypes()
        {
            try
            {
                //TODO: GET BOOM EQUIPMENT LIST TYPES
                //var types = new List<BoomEquipmentListTypeDto>();
                //var types = _mpwWebService.GetCurrentEnabledEquipmentOnSite(account);
                AuthenticateWebService();
                CustomerProfileDTO customerProfile = _mpwWebService.GetCustomerProfile("57076");
                var account = Guid.Parse(HttpContext.Current.Request.QueryString["account"]);
                //PLACEHOLDER
                var list = new SelectList(
                    new List<SelectListItem>
                    {
                        new SelectListItem { Selected = true, Value = "", Text = "Select" },
                        new SelectListItem { Value = "Type 1", Text = "Value 1" },
                        new SelectListItem { Value = "Type 2", Text = "Value 2" },
                        new SelectListItem { Value = "Type 3", Text = "Value 3" }
                    }, "Value", "Text"
                );
                return list;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new SelectList
                (
                    new List<SelectListItem>
                    {
                        new SelectListItem { Value = "Type Error", Text = "Type Error" }
                    },
                    "Value", "Text"
                );
            }
        }

        #endregion Boom Equipment

        #region DI Equipment

        public DiEquipmentDto GetDiEquipmentData(string account, int size, int index, DateTime? start, DateTime? end,
            bool refreshCache = false)
        {
            try
            {
                var model = new DiEquipmentDto
                {
                    WaterDirectNavigation = WaterDirectNavigationHelper.DefaultPermissions,
                    ScheduledJobs = new List<ScheduledJobDTO>(),
                    PageSize = size,
                    PageIndex = index,
                    PageSizeList = new SelectList(new List<int>
                    {
                        20, 25, 30, 35, 40, 50, 60
                    }),
                    PagesPerSet = 10,
                    StartDate = start,
                    EndDate = end
                };

                //get user account list from znode
                WaterDirectUserAccountListDto userAccountList = GetUserAccountZNodeDetails(9);
                //pull jdenumbers from znode data
                List<string> jdeNumbers = GetUserAssociatedCustomerJdeNumbers(userAccountList);
                //get wmc customer accounts using znode jde numbers
                List<CustomerDetailsDTO> wmcFacilities = GetUserAssociatedCustomerAccounts(jdeNumbers);
                model.WaterDirectNavigation.WaterDirectNavPermissions.CanViewMobileEquipment = false;
                model.WaterDirectNavigation.UserAssignedAccountsSelectList = GetUserAssignedAccountsSelectList(wmcFacilities);
                model.WaterDirectNavigation.SelectedUserAssignedAccount = account;
                model = GetOrderHistory(model, true);
                return model;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private DiEquipmentDto GetOrderHistory(DiEquipmentDto model, bool refreshCache = false)
        {
            string account = model.WaterDirectNavigation.SelectedUserAssignedAccount;
            if (string.IsNullOrWhiteSpace(account))
            {
                return model;
            }

            model.ScheduledJobs = GetCachedScheduledJobs(account, refreshCache);
            model = FilterScheduledJobs(model);
            model = CalculateOrderHistoryPaging(model);

            return model;
        }

        private List<ScheduledJobDTO> GetCachedScheduledJobs(string account, bool refreshCache = false)
        {
            List<ScheduledJobDTO> data = HttpContext.Current.Cache.Get("DiOrderHistory") as List<ScheduledJobDTO>;

            if (data == null || refreshCache)
            {
                string facilityId = account.Split(',')[0];
                Guid customerFacilityId;
                if (Guid.TryParse(facilityId, out customerFacilityId))
                {
                    data = _mpwWebService.GetScheduledJobs(customerFacilityId, null, null);
                    HttpContext.Current.Cache.Insert("DiOrderHistory", data);
                }
                else
                {
                    Console.WriteLine("Selected Account is not a valid Id.");
                }
            }

            return data;
        }

        private DiEquipmentDto FilterScheduledJobs(DiEquipmentDto model)
        {
            if (model.StartDate.HasValue && model.EndDate.HasValue)
            {
                DateTime start = model.StartDate.Value;
                DateTime end = model.EndDate.Value;
                model.ScheduledJobs = model.ScheduledJobs
                    .Where(x => x.OrderDate.Date >= start.Date && x.OrderDate <= end.Date).ToList();
            }

            return model;
        }

        private DiEquipmentDto CalculateOrderHistoryPaging(DiEquipmentDto model)
        {
            model.PageCount = (model.ScheduledJobs.Count + model.PageSize - 1) / model.PageSize;
            model.PageSets = (model.PageCount + model.PagesPerSet - 1) / model.PagesPerSet;
            for (int i = 0; i < model.PageSets; i++)
            {
                int min = i * 10;
                int max = (i + 1) * 10;
                if (model.PageIndex >= min && model.PageIndex < max)
                {
                    model.PageMin = min;
                    model.PageMax = max > model.PageCount ? model.PageCount : max;
                    break;
                }
            }

            model.ScheduledJobs = model.ScheduledJobs.OrderByDescending(x => x.OrderNumber)
                .Skip(model.PageSize * model.PageIndex).Take(model.PageSize).ToList();

            return model;
        }

        public DiWorkOrderDto GetDiWorkOrderData(string account, Guid workOrderId, string backUrl)
        {
            var model = new DiWorkOrderDto
            {
                WaterDirectNavigation = WaterDirectNavigationHelper.DefaultPermissions,
                Workorder = _mpwWebService.GetWorkOrderByID(workOrderId.ToString()),
                BackUrl = backUrl
            };
            //get user account list from znode
            WaterDirectUserAccountListDto userAccountList = GetUserAccountZNodeDetails(9);//needs userid
            //pull jdenumbers from znode data
            List<string> jdeNumbers = GetUserAssociatedCustomerJdeNumbers(userAccountList);
            //get wmc customer accounts using znode jde numbers
            List<CustomerDetailsDTO> wmcFacilities = GetUserAssociatedCustomerAccounts(jdeNumbers);
            model.WaterDirectNavigation.WaterDirectNavPermissions.CanViewMobileEquipment = false;
            model.WaterDirectNavigation.UserAssignedAccountsSelectList = GetUserAssignedAccountsSelectList(wmcFacilities);
            model.WaterDirectNavigation.SelectedUserAssignedAccount = account;
            model.Workorder.Load = GetLoadForWorkOrder(account, workOrderId);
            return model;
        }

        public DiOrderDto GetDiOrderData(string account, string backUrl)
        {
            string facilityId = account.Split(',')[0];
            CustomerDetailsDTO customerDetails = GetCustomerDetailFromCustomerProfiles(Guid.Parse(facilityId));

            var model = new DiOrderDto
            {
                WaterDirectNavigation = WaterDirectNavigationHelper.DefaultPermissions,
                BackUrl = backUrl,
                DILoads = new SelectList(customerDetails.DILoads, "LoadID", "LoadName"),
                DefaultAccessoryEquipment = customerDetails.AccessoryEquipment
            };
            model.WaterDirectNavigation.WaterDirectNavPermissions.CanViewMobileEquipment = false;
            model.WaterDirectNavigation.UserAssignedAccountsSelectList = GetUserAssignedAccountsSelectList(new List<CustomerDetailsDTO>());
            model.WaterDirectNavigation.SelectedUserAssignedAccount = account;
            return model;
        }

        public int SubmitDiOrder(int userId, string accountId, bool isSwap, string deliverOption, DateTime? deliveryDate,
            string PONumber, Guid selectedDILoadId, int trailerQuantity, string orderNotes)
        {
            string facilityId = accountId.Split(',')[0];
            string jdeNumber = accountId.Split(',')[1];

            CustomerDetailsDTO customerDetails = GetCustomerDetailFromCustomerProfiles(Guid.Parse(facilityId));
            //WaterDirectUserAccountDetailsDto userAccountDetails = GetUserAccountWmcDetails(userId).UserAccountDetails;

            //Eventually change WMC external service to accept only one order object
            List<CustomerOrderDTO> orders = new List<CustomerOrderDTO>
            {
                new CustomerOrderDTO
                {
                    CustomerFacilityID = Guid.Parse(facilityId),
                    CustomerFacilityName = customerDetails.CustomerFacilityName,
                    JDENumber = int.Parse(jdeNumber),
                    InitiatorType = isSwap ? "Swap" : "Delivery",
                    DeliveryLevel = int.Parse(deliverOption),
                    RequestedDate = deliveryDate,
                    PONumber = PONumber,
                    LoadID = selectedDILoadId,
                    Quantity = trailerQuantity,
                    CustomerNotes = orderNotes
                    // ZnodeID = int.Parse(userAccountDetails.UserId),
                    // ZnodeUserEmail = userAccountDetails.Email,
                    // ZnodeUserFirstName = userAccountDetails.FirstName,
                    // ZnodeUserLastName = userAccountDetails.LastName,
                    // ZnodeUserPhoneNumber = userAccountDetails.PhoneNumber
                }
            };

            int confirmationNumber = _mpwWebService.CreateOrder(orders);
            return confirmationNumber;
        }
        private string GetLoadForWorkOrder(string account, Guid workOrderId)
        {
            ScheduledJobDTO job = GetCachedScheduledJobs(account).SingleOrDefault(x => x.WorkOrderID == workOrderId);
            return job != null ? job.LoadName : "N/A";
        }

        #endregion DI Equipment

        #region Mobile Equipment

        public MobileEquipmentDto GetMobileEquipmentData(int userId)
        {
            try
            {
                var data = new MobileEquipmentDto
                {
                    WaterDirectNavigation = WaterDirectNavigationHelper.DefaultPermissions
                };
                //get user account list from znode
                WaterDirectUserAccountListDto userAccountList = GetUserAccountZNodeDetails(userId);
                //pull jdenumbers from znode data
                List<string> jdeNumbers = GetUserAssociatedCustomerJdeNumbers(userAccountList);
                //get wmc customer accounts using znode jde numbers
                List<CustomerDetailsDTO> wmcFacilities = GetUserAssociatedCustomerAccounts(jdeNumbers);
                data.WaterDirectNavigation.WaterDirectNavPermissions.CanViewMobileEquipment = false;
                data.WaterDirectNavigation.UserAssignedAccountsSelectList = GetUserAssignedAccountsSelectList(wmcFacilities);
                return data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #endregion Mobile Equipment

        #region User
        /// <summary>
        /// Gets all associated user customer accounts, from wmc, using jdeNumbers from ZNode
        /// </summary>
        /// <param name="jdeNumbers"></param>
        /// <returns></returns>
        private List<CustomerDetailsDTO> GetUserAssociatedCustomerAccounts(List<string> jdeNumbers)
        {
            //todo: switch to this when we have a new endpoint to GetCustomerProfilesByList(List<string> jdeNumbers)
            AuthenticateWebService();
            List<CustomerDetailsDTO> accounts = new List<CustomerDetailsDTO>();
            List<CustomerProfileDTO> customerProfiles = GetCachedCustomerProfiles(jdeNumbers);
            customerProfiles.ForEach(x =>
            {
                accounts.AddRange(x.CustomerFacilities);
            });
            //var accounts = customerProfile.CustomerFacilities;
            return accounts;
        }
        /// <summary>
        /// Gets all jde numbers from associated user customer accounts, from znode
        /// </summary>
        /// <param name="userAccountList"></param>
        /// <returns></returns>
        private List<string> GetUserAssociatedCustomerJdeNumbers(WaterDirectUserAccountListDto userAccountList)
        {
            //todo: https://preprod-sales-api.mpwservices.com/mpwaccount/getuseraccountdetails/9
            List<string> jdeNumbers = userAccountList.UserAccountDetails.AccountDetails.Select(x => x.ExternalId)
                .ToList();
            return jdeNumbers;
        }

        private WaterDirectUserAccountListDto GetUserAccountZNodeDetails(int userId)
        {
            string userAccountDetailList = _zNodeApiClient.GetUserAccountDetails(userId);
            var accountDetailCollection = JsonConvert
                .DeserializeObject<WaterDirectUserAccountListDto>(userAccountDetailList);
            return accountDetailCollection;
        }

        private SelectList GetUserAssignedAccountsSelectList(List<CustomerDetailsDTO> wmcFacilities)
        {
            try
            {
                AuthenticateWebService();

                var customerProfiles = GetCachedCustomerProfiles(true);
                var selectListItems = new List<SelectListItem>
                {
                    new SelectListItem { Value = "", Text = "Select Account" }
                };
                wmcFacilities.ForEach((x =>
                {
                    var item = new SelectListItem { Value = x.CustomerFacilityID.ToString(), Text = x.CustomerFacilityName };
                    selectListItems.Add(item);
                }));
                //List<CustomerProfileDTO> customerProfile = GetCachedCustomerProfiles();
                //var accounts = customerProfile.CustomerFacilities;

                //accounts.ForEach((x =>
                //{
                //    selectListItems.Add(new SelectListItem
                //    { Value = x.CustomerFacilityID.ToString(), Text = x.CustomerFacilityName });
                //}));
                var selectList = new SelectList(selectListItems, "Value", "Text", "");
                return selectList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new SelectList
                (
                    new List<SelectListItem>
                    {
                        new SelectListItem { Value = "Account Error", Text = "Account Error" }
                    },
                    "Value", "Text"
                );
            }
        }

        private List<CustomerProfileDTO> GetCachedCustomerProfiles(bool refreshCache = false)
        {
            List<CustomerProfileDTO> customerProfiles =
                HttpContext.Current.Cache.Get("WaterDirectCustomerProfile") as List<CustomerProfileDTO>;
            if (customerProfiles == null || !customerProfiles.Any() || refreshCache)
            {
                //todo: https://preprod-sales-api.mpwservices.com/mpwaccount/getuseraccountdetails/9
                //returns list of user's assigned accounts 
                //var jdeNumbers =
                //var assignedAccounts =
                //This will change when multiple accounts per user feature gets implemented.
                string[] jdeNumbers = { "57076", "21937" };

                customerProfiles = _mpwWebService.GetCustomerProfilesByList(jdeNumbers);
                HttpContext.Current.Cache.Insert("WaterDirectCustomerProfiles", customerProfiles);
            }

            return customerProfiles;
        }
        private List<CustomerProfileDTO> GetCachedCustomerProfiles(List<string> jdeNumbers, bool refreshCache = false)
        {
            List<CustomerProfileDTO> customerProfiles =
                HttpContext.Current.Cache.Get("WaterDirectCustomerProfile") as List<CustomerProfileDTO>;
            if (customerProfiles == null || !customerProfiles.Any() || refreshCache)
            {
                string[] jdeList = (jdeNumbers.Count == 0)
                    ? new string[2]{ "57076", "21937"}
                    : jdeNumbers.ToArray();
                customerProfiles = _mpwWebService.GetCustomerProfilesByList(jdeList);
                HttpContext.Current.Cache.Insert("WaterDirectCustomerProfiles", customerProfiles);
            }
            return customerProfiles;
        }

        private List<CustomerDetailsDTO> GetAllCustomerDetailsDTOs(bool refreshCache = false)
        {
            List<CustomerProfileDTO> profiles = GetCachedCustomerProfiles(refreshCache);
            List<CustomerDetailsDTO> details = new List<CustomerDetailsDTO>();
            foreach (CustomerProfileDTO profile in profiles)
            {
                details.AddRange(profile.CustomerFacilities);
            }

            return details;
        }

        private CustomerDetailsDTO GetCustomerDetailFromCustomerProfiles(Guid customerFacilityId,
            bool refreshCache = false)
        {
            return GetAllCustomerDetailsDTOs(refreshCache)
                .SingleOrDefault(x => x.CustomerFacilityID == customerFacilityId);
        }

        private void AuthenticateWebService()
        {
            try
            {
                bool retval = _mpwWebService.GetAuthenticationToken();
                Console.WriteLine($"MPW Web Service Authentication Status: {(retval ? "Success" : "Failed")}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion User
    }
}