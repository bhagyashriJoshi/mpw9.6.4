﻿using System.Web.Mvc;
using Znode.Engine.WebStore.Controllers;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class WdBaseController : BaseController
    {
        /// <summary>
        /// Authenticates user and prevents deep links being accessible
        /// This is based on the user being signed in, selecting a customer facility,
        /// and that facility's section (DI, Mobile, Boom) not being disabled.
        /// </summary>
        /// <returns></returns>
        public bool IsAuthenticated(bool pageIsEnabled)
        {
            //landing page is always allowed
            var action = this.ControllerContext.RouteData.Values["action"].ToString().ToLower();
            var controller = this.ControllerContext.RouteData.Values["controller"].ToString().ToLower();
            if (controller == "waterdirect" && action == "index") return true;
            //user must be authenticated if this isn't the landing page
            if (!User.Identity.IsAuthenticated) return false;
            //any other page must be authenticated with a required customer facility id in the url
            string account = Request.QueryString.Get("account");
            if (string.IsNullOrEmpty(account)) return false;
            //customer facility section must be enabled
            return pageIsEnabled;
        }

        public RedirectToRouteResult LandingPageView()
        {
            return RedirectToAction("Index", "WaterDirect");
        }
    }
}
