﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.MPW.Service.DataModels;

namespace Znode.WebStore.Custom.ViewModel
{
    public class DiEquipmentDto : BaseDto
    {
        public List<ScheduledJobDTO> ScheduledJobs { get; set; }
        public int PageSize { get; set; }
        public SelectList PageSizeList { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public int PageMin { get; set; }
        public int PageMax { get; set; }
        public int PagesPerSet { get; set; }
        public int PageSets { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool RefreshCache { get; set; }
    }
}