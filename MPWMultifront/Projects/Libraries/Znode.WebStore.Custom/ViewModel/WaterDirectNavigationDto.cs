﻿using System.Web.Mvc;

namespace Znode.WebStore.Custom.ViewModel
{
    public class WaterDirectNavigationDto
    {
        public WaterDirectNavPermissionsDto WaterDirectNavPermissions { get; set; }
        public SelectList UserAssignedAccountsSelectList { get; set; }
        public string SelectedUserAssignedAccount { get; set; }

        public bool IsAllDisabled
        {
            get
            {
                bool isDisabled = WaterDirectNavPermissions.CanViewDiEquipment == false
                                  && WaterDirectNavPermissions.CanViewMobileEquipment == false
                                  && WaterDirectNavPermissions.CanViewBoomEquipment == false;
                return isDisabled;
            }
        }
    }
}
