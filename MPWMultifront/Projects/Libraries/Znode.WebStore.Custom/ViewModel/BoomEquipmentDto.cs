﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Znode.WebStore.Custom.ViewModel
{
    public class BoomEquipmentDto : BaseDto
    {
        public string SelectedBoomEquipmentListType { get; set; }
        public List<BoomFileDownloadDto> BoomFileDownloads { get; set; }
        public SelectList BoomEquipmentListTypes { get; set; }
    }
}
