﻿namespace Znode.WebStore.Custom.ViewModel.Enums
{
    public enum DeliveryOptions
    {
        SpecificDate = 0,
        TwentyFourHours = 24
    }
}