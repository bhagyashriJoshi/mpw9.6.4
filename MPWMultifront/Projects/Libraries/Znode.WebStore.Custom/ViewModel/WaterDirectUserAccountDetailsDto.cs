﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

//    "AccountDetails":[
         
//],
//"UserId":"9",
//"FirstName":"Anthony",
//"LastName":"Williams",
//"FullName":null,
//"UserName":"awilliams@mpwservices.com",
//"Email":"awilliams@mpwservices.com",
//"PhoneNumber":null,
//"PortalId":0,
//"CreatedBy":0,
//"CreatedDate":"0001-01-01T00:00:00",
//"ModifiedBy":0,
//"ModifiedDate":"0001-01-01T00:00:00",
//"ActionMode":null,
//"Custom1":null,
//"Custom2":null,
//"Custom3":null,
//"Custom4":null,
//"Custom5":null
//}
namespace Znode.WebStore.Custom.ViewModel
{
    public class WaterDirectUserAccountDetailsDto
    {
        public List<WaterDirectUserAccountDetailDto> AccountDetails { get; set; }

        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int? PortalId { get; set; }

        [JsonIgnore]
        public int CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTime ModifiedDate { get; set; }

        public string ActionMode { get; set; }

        public string Custom1 { get; set; }

        public string Custom2 { get; set; }

        public string Custom3 { get; set; }

        public string Custom4 { get; set; }

        public string Custom5 { get; set; }

    }
}
