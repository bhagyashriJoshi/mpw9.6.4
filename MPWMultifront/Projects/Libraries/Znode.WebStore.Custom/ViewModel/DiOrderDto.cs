﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.MPW.Service.DataModels;
using Znode.WebStore.Custom.Helper;
using Znode.WebStore.Custom.ViewModel.Enums;

namespace Znode.WebStore.Custom.ViewModel
{
    public class DiOrderDto : BaseDto
    {
        public string BackUrl { get; set; }
        public SelectList DILoads { get; set; }
        [Required(ErrorMessage = "Please select a DI Load.")] public LoadConfigDTO SelectedDILoad { get; set; }
        [Required(ErrorMessage = "Please enter a PO Number.")] public string PONumber { get; set; }
        public bool IsSwap { get; set; }
        [Required(ErrorMessage = "Please select a Delivery Option.")] public string DeliveryOption { get; set; }

        [RequiredIf("DeliveryOption", DeliveryOptions.SpecificDate, ErrorMessage = "Please select a Delivery Date.")]
        [DateGreaterThanToday]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public string DeliveryDate { get; set; }

        [Range(1, 100, ErrorMessage = "Trailer Quantity must be between 1 and 100.")] public int TrailerQuantity { get; set; }
        public string OrderNotes { get; set; }
        public AccessoryEquipmentGroupDTO DefaultAccessoryEquipment { get; set; }
    }
}