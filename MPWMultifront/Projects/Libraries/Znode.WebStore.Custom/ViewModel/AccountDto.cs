﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.WebStore.Custom.Data.Models
{
    public class AccountDto
    {
        public string CompanyName { get; set; }
        public int AccountId { get; set; }
    }
}
