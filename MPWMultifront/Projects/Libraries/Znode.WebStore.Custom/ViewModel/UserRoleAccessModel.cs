﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.WebStore.Custom.ViewModel
{
    public class UserRoleAccessModel : BaseModel
    {
        public int AccountTypeId { get; set; }
        public int ParentAccountId { get; set; }
        public string AccountTypeName { get; set; }
        public bool IsConfirmed { get; set; }
        public string RoleName { get; set; }
        public bool? Status { get; set; }
        public string AccessName { get; set; }
        public int AccessId { get; set; }
        public string DepartmentName { get; set; }
        public string AccountName { get; set; }
        public string ParentExternalId { get; set; }
        public string ExternalId { get; set; }
        public string PhoneNumber { get; set; }
        public Collection<AddressModel> ParentAccountAddresses { get; set; }
        public decimal? MaxBudget { get; set; }
        public bool? EnableDiTrailer { get; set; }
        public bool? EnableBoom { get; set; }
        public bool? EnableMobileEquipment { get; set; }
        public bool? EnableConditionalFormatting { get; set; }
        public UserRoleAccessModel Sample()
        {
            return new UserRoleAccessModel
            {
                AccessName = "Does not require approval",
                IsConfirmed = false,
                RoleName = "CompanyAdmin",
                Status = false
            };
        }
    }
}
