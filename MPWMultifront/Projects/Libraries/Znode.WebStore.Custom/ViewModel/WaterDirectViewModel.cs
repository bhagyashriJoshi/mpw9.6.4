﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.WebStore.Custom.ViewModel
{
    public class WaterDirectViewModel
    {
        public WaterDirectDropdownViewModel AccountList { get; set; }
        public bool EnableDI { get; set; }
        public bool EnableMobileEquipment { get; set; }
        public bool EnableConditionalFormatting { get; set; }
        public bool EnableBoom { get; set; }
    }
}
