﻿namespace Znode.WebStore.Custom.ViewModel
{
    public class WaterDirectNavPermissionsDto
    {
        public bool CanViewDiEquipment { get; set; }
        public bool CanViewMobileEquipment { get; set; }
        public bool CanViewBoomEquipment { get; set; }
    }
}
