﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.WebStore.Custom.ViewModel
{
    public class AddressModel : BaseModel
    {
        public int AccountId { get; set; }
        public int AddressId { get; set; }
        public string City { get; set; }
        public string CompanyName { get; set; }
        public string CountryCode { get; set; }
        public string FirstName { get; set; }
        public bool IsDefaultBilling { get; set; }
        public bool IsDefaultShipping { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string PostalCode { get; set; }
        public string StateCode { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public bool ValidAddress { get; set; }
        public string Email { get; set; }
        public bool SetUserBillingAddress { get; set; }

        public AddressModel Sample()
        {
            return new AddressModel
            {
                AccountId = 11521,
                AddressId = 2,
                City = "Fake City",
                CompanyName = "Your Company",
                CountryCode = "US",
                FirstName = "Site",
                IsDefaultBilling = true,
                IsDefaultShipping = true,
                LastName = "Admin",
                MiddleName = "",
                Name = "Default Billing & Shipping",
                PhoneNumber = "1-888-Your-Store",
                PostalCode = "12345",
                StateCode = "SD",
                StreetAddress1 = "123 Fake Street",
                StreetAddress2 = ""
            };
        }
    }
}
