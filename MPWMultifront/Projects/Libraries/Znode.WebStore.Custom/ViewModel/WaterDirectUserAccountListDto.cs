﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

//    "UserAccountDetails":{
//"AccountDetails":[
         
//],
//    "ErrorCode":null,
//    "ErrorMessage":null,
//    "HasError":false,
//    "CustomModelState":null,
//    "CreatedBy":0,
//    "CreatedDate":"0001-01-01T00:00:00",
//    "ModifiedBy":0,
//    "ModifiedDate":"0001-01-01T00:00:00",
//    "ActionMode":null,
//    "Custom1":null,
//    "Custom2":null,
//    "Custom3":null,
//    "Custom4":null,
//    "Custom5":null
//}
namespace Znode.WebStore.Custom.ViewModel
{
    public class WaterDirectUserAccountListDto
    {
        public WaterDirectUserAccountDetailsDto UserAccountDetails { get; set; }

        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool HasError { get; set; }
        public string CustomModelState { get; set; }

        [JsonIgnore]
        public int CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        public int ModifiedBy { get; set; }

        [JsonIgnore]
        public DateTime ModifiedDate { get; set; }

        public string ActionMode { get; set; }

        public string Custom1 { get; set; }

        public string Custom2 { get; set; }

        public string Custom3 { get; set; }

        public string Custom4 { get; set; }

        public string Custom5 { get; set; }
    }
}
