﻿using Znode.MPW.Service.DataModels;

namespace Znode.WebStore.Custom.ViewModel
{
    public class DiWorkOrderDto : BaseDto
    {
        public CustomerWorkorder Workorder { get; set; }
        public string BackUrl { get; set; }
    }
}