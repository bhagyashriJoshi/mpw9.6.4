﻿namespace Znode.WebStore.Custom.ViewModel
{
    public class BoomFileDownloadDto
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
