﻿using Autofac;
using Znode.Libraries.Framework.Business;
using Znode.MPW.Service;
using Znode.MPW.Service.Factories;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.Engine.WebStore
{
    public class CustomDependancyRegistration : IDependencyRegistration
    {
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<WaterDirectData>().As<IWaterDirectData>().InstancePerDependency();
            builder.RegisterType<ZNodeApiClient>().As<IZNodeApiClient>().InstancePerDependency();
            builder.RegisterType<MPWWebService>().As<IMPWWebService>().InstancePerDependency();
            builder.RegisterType<HttpClientFactory>().As<IHttpClientFactory>().InstancePerDependency();
        }
        public int Order
        {
            get { return 1; }
        }
    }
}