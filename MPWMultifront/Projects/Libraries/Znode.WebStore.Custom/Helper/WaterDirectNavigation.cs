﻿using System.Collections.Generic;
using System.Linq;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Helper
{
    public static class WaterDirectNavigationHelper
    {
        /// <summary>
        /// default permissions all set to false
        /// </summary>
        public static WaterDirectNavigationDto DefaultPermissions => new WaterDirectNavigationDto
        {
            WaterDirectNavPermissions = new WaterDirectNavPermissionsDto
            {
                CanViewBoomEquipment = false,
                CanViewDiEquipment = true,
                CanViewMobileEquipment = false
            }
        };

        public static WaterDirectNavigationDto UserPermissions(WaterDirectUserAccountListDto userAccountList, string accountId)
        {
            List<WaterDirectUserAccountDetailDto> accountDetails = userAccountList?.UserAccountDetails?.AccountDetails;
            if (accountDetails == null || accountDetails.Count == 0) return DefaultPermissions;
            string jdeNumber = accountId.Split(',')[1];
            var permissions = new WaterDirectNavigationDto
            {
                WaterDirectNavPermissions = new WaterDirectNavPermissionsDto
                {
                    CanViewBoomEquipment = IsEnabled(jdeNumber, WaterDirectSection.Boom, accountDetails),
                    CanViewDiEquipment = IsEnabled(jdeNumber, WaterDirectSection.DI, accountDetails),
                    CanViewMobileEquipment = IsEnabled(jdeNumber, WaterDirectSection.Mobile, accountDetails)
                }
            };
            return permissions;
        }

        private static bool IsEnabled(string jdeNumber, WaterDirectSection section, List<WaterDirectUserAccountDetailDto> userAccountDetails)
        {
            bool isEnabled = false;
            switch (section)
            {
                case WaterDirectSection.DI:
                    isEnabled = userAccountDetails.FirstOrDefault(x => x.ExternalId == jdeNumber)?.DITrailerEnabled == "true";
                    break;
                case WaterDirectSection.Mobile:
                    isEnabled = userAccountDetails.FirstOrDefault(x => x.ExternalId == jdeNumber)?.MobileEquipmentEnabled == "true";
                    break;
                case WaterDirectSection.Boom:
                    isEnabled = userAccountDetails.FirstOrDefault(x => x.ExternalId == jdeNumber)?.BoomEnabled == "true";
                    break;
            }
            return isEnabled;
        }

        private enum WaterDirectSection
        {
            Boom,
            Mobile,
            DI
        }
    }
}
