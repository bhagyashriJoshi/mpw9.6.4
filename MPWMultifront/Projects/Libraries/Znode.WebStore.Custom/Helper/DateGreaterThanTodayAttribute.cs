﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Znode.WebStore.Custom.Helper
{
    public class DateGreaterThanTodayAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            DateTime dt = (DateTime)value;
            if (dt.ToUtcDateTime() > DateTime.UtcNow)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(ErrorMessage ?? "Date must be after than today");
        }
    }
}