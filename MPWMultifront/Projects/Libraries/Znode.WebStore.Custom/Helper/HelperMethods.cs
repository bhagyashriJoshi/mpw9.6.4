﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.WebStore.Custom.Helper
{
    public static class HelperMethods
    {
        /// <summary>
        /// To get image path with url
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static string GetImagePath(string image)
        {

            if (!string.IsNullOrEmpty(image))
            {
                return image.Replace("~", ConfigurationManager.AppSettings["ZnodeApiRootUri"]);
            }
            return string.Empty;
        }

        /// <summary>
        /// To get file path with url
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string GetFilePath(string file)
        {

            if (!string.IsNullOrEmpty(file))
            {
                StringBuilder filePath = new StringBuilder();
                filePath.Append(ConfigurationManager.AppSettings["ZnodeApiRootUri"]);
                filePath.Append(file);
                return filePath.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// To get relative path of image 
        /// </summary>
        /// <param name="image">string image path</param>
        /// <returns>returns relative path of image </returns>
        public static string GetImageRelativePath(string image)
        {
            if (!string.IsNullOrEmpty(image))
            {
                return image.Replace("~", string.Empty);
            }
            return string.Empty;
        }

        /// <summary>
        /// Get the enable/disable checkmark icon
        /// </summary>
        /// <param name="isEnabled">Indicates to icon enabled or disabled status.</param>
        /// <returns>Returns enabled/disabled checkmark icon.</returns>
        public static string GetCheckMark(bool isEnabled)
        {
            string icon = string.Empty;

            if (!Equals(isEnabled, null))
            {
                if (isEnabled)
                {
                    icon = "glyphicon glyphicon-ok";
                }
                else
                {
                    icon = "glyphicon glyphicon-remove";
                }
            }
            return icon;
        }

        public static string GetDomainUrl()
        {
            return (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
        }

        public static bool IsDebugMode
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["IsDebugMode"]);
            }
        }

        public static bool EnableChartReport
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableChartReport"]);
            }
        }

        public static int GridPagingStartValue
        {
            get
            {
                return string.IsNullOrEmpty(ConfigurationManager.AppSettings["GridPagingStartValue"]) ? 10 : Convert.ToInt32(ConfigurationManager.AppSettings["GridPagingStartValue"]);
            }
        }

        /// <summary>
        /// To get formatted price 
        /// </summary>
        /// <param name="price">decimal price</param>
        /// <returns>returns two decimal value</returns>
        public static decimal FormatPrice(decimal? price)
        {
            return Equals(price, null) ? 0 : Math.Round(price.Value, 2);
        }

        /// <summary>
        /// To get formatted price
        /// </summary>
        /// <param name="price">double price</param>
        /// <returns>returns string with two decimal value</returns>
        public static string FormatPrice(double? price)
        {
            return Equals(price, null) ? string.Empty : String.Format("{0:0.00}", price);
        }

        /// <summary>
        /// To get Format Date as per date format define in web. config file
        /// </summary>
        /// <param name="date">DateTime date</param>
        /// <returns>returns string FormatDate</returns>
        public static string ViewDateFormat(DateTime date)
        {
            string dateFormat = Convert.ToString(ConfigurationManager.AppSettings["StringDateFormat"]);
            if (string.IsNullOrEmpty(dateFormat))
            {
                dateFormat = "MM/dd/yyyy";
            }
            return Equals(date, null) ? string.Empty : date.ToString(dateFormat);
        }


        /// <summary>
        /// To get Format Date time as per date format define in web. config file
        /// </summary>
        /// <param name="date">DateTime date</param>
        /// <returns>returns string FormatDateTime</returns>
        public static string ViewDateFormatWithTime(DateTime date)
        {
            string dateFormat = Convert.ToString(ConfigurationManager.AppSettings["StringDateTimeFormat"]);
            if (string.IsNullOrEmpty(dateFormat))
            {
                dateFormat = "MM/dd/yyyy H:mm:ss";
            }
            return Equals(date, null) ? string.Empty : date.ToString(dateFormat);
        }

        /// <summary>
        /// To get Format Date as per date formate define in web. config file
        /// </summary>
        /// <param name="date">DateTime date</param>
        /// <returns>>returns string FormatDate</returns>
        public static string SearchDateFormat(DateTime date)
        {
            string dateFormat = Convert.ToString(ConfigurationManager.AppSettings["StringSearchDateFormat"]);
            if (string.IsNullOrEmpty(dateFormat))
            {
                dateFormat = "dd/MM/yyyy";
            }
            return Equals(date, null) ? string.Empty : date.ToString(dateFormat);
        }

        public static string DatePickDateFormat()
        {
            return Convert.ToString(ConfigurationManager.AppSettings["DatePickDateFormat"]);
        }

        public static string ConvertStringToDate(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                return SearchDateFormat(Convert.ToDateTime(date));
            }
            return string.Empty;
        }

        public static string GetStringDateFormat()
        {
            return Convert.ToString(ConfigurationManager.AppSettings["StringDateFormat"]);
        }

        public static string GetStringDateTimeFormat()
        {
            return Convert.ToString(ConfigurationManager.AppSettings["StringDateTimeFormat"]);
        }

        /// <summary>
        /// To get formatted price and return nullable decimal
        /// </summary>
        /// <param name="price">decimal price</param>
        /// <returns>returns two decimal value</returns>
        public static decimal? FormatNullablePrice(decimal? price)
        {
            return Equals(price, null) ? 0 : Math.Round(price.Value, 2);
        }

        /// <summary>
        /// Returns substring of a given string.
        /// </summary>
        /// <param name="value">string value</param>
        /// <param name="maxLength">length of substring</param>
        /// <returns>returns substring of specified length</returns>
        public static string Truncate(this string value, int maxLength)
        {
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        /// <summary>
        /// To get formatted price with current currency symbol and suffix
        /// </summary>
        /// <param name="price">decimal price</param>
        /// <returns>returns string with two decimal value and current currency symbol</returns>
        public static string FormatPriceWithCurrency(decimal? price)
        {
            string currencySuffix = string.IsNullOrEmpty(PortalAgent.CurrentPortal.CurrencySuffix) ? string.Empty : string.Format("({0})", PortalAgent.CurrentPortal.CurrencySuffix);
            CultureInfo info = new CultureInfo(PortalAgent.CurrentPortal.CurrencyName);
            string currencyValue = string.Format("{0}{1}", price.GetValueOrDefault().ToString("c", info.NumberFormat), currencySuffix);
            return currencyValue;
        }

        /// <summary>
        /// To get formatted price with current currency symbol and suffix
        /// </summary>
        /// <param name="price">double price</param>
        /// <returns>returns string with two decimal value and current currency symbol</returns>
        public static string FormatPriceWithCurrency(double? price)
        {
            string currencySuffix = string.IsNullOrEmpty(PortalAgent.CurrentPortal.CurrencySuffix) ? string.Empty : string.Format("({0})", PortalAgent.CurrentPortal.CurrencySuffix);
            CultureInfo info = new CultureInfo(PortalAgent.CurrentPortal.CurrencyName);
            string currencyValue = string.Format("{0}{1}", price.GetValueOrDefault().ToString("c", info.NumberFormat), currencySuffix);
            return currencyValue;
        }

        /// <summary>
        /// To get formatted current currency symbol.
        /// </summary>
        /// <param name="price">decimal price</param>
        /// <returns>returns string with current currency symbol.</returns>
        public static string GetCurrency(decimal? price)
        {
            CultureInfo info = new CultureInfo(PortalAgent.CurrentPortal.CurrencyName);
            return info.NumberFormat.CurrencySymbol;
        }
    }
}
