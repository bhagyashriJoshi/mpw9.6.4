﻿using System.Configuration;

namespace Znode.WebStore.Custom.Helper
{
    public static class ConfigHelper
    {
        public static string ApiHost => Get("ApiHost");
        public static string OldApiHost => Get("OldApiHost");
        public static string ApiPw => Get("ApiPw");

        private static string Get(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }
    }
}
