﻿using System;
using System.Collections.Generic;

namespace Znode.MPW.Service.DataModels
{
    /// <summary>
    ///     Summary description for CustomerWorkOrder
    /// </summary>
    public class CustomerWorkorder
    {
        public int CustomerJDE { get; set; }
        public string CustomerName { get; set; }
        public Guid CustomerFacilityID { get; set; }
        public string DeliveryDriverName { get; set; }
        public string PickupDriverName { get; set; }
        public string Load { get; set; }
        public string WorkorderNumber { get; set; }
        public string TrailerUnitNumber { get; set; }
        public string TrailerSize { get; set; }
        public string WorkorderTypeName { get; set; }
        public string AssociatedCostCenterName { get; set; }
        public DateTime? OnSiteDate { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? OffSiteDate { get; set; }
        public decimal? ConductivityInitial { get; set; }
        public decimal? ConductivitySetpoint { get; set; }
        public decimal? TDSFinal { get; set; }
        public decimal? PreMBCond { get; set; }
        public decimal? MBCond { get; set; }
        public decimal? InletTurbidity { get; set; }
        public decimal? Flow { get; set; }
        public decimal? PressureIn { get; set; }
        public decimal? PressureOut { get; set; }
        public long? GallonageInitial { get; set; }
        public long? GallonageFinal { get; set; }
        public string CustomerPOName { get; set; }
        public decimal? CO2ScaleID { get; set; }
        public int? NumberOfDrops { get; set; }
        public List<CustomerWorkorderDescLine> DescriptionLines { get; set; }
        public List<CustomerWorkorderLineItem> LineItems { get; set; }
        public List<CustomerWorkorderSignOff> Signatures { get; set; }
        public AccessoryEquipmentGroupDTO AccessoryEquipmentDelivered { get; set; }
        public AccessoryEquipmentGroupDTO AccessoryEquipmentReturned { get; set; }
    }
}
