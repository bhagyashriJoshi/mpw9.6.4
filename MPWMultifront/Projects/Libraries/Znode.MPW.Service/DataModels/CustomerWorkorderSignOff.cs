﻿using System;

namespace Znode.MPW.Service.DataModels
{
    public class CustomerWorkorderSignOff
    {
        public string SignOffTypeName { get; set; }
        public byte[] CustomerSignature { get; set; }
        public DateTime? SignatureTimestamp { get; set; }
        public bool IsMobile { get; set; }
        public string EmployeeName { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerTitle { get; set; }
        public string CustomerEmail { get; set; }
        public string ControlSwitchPosition { get; set; }
    }
}
