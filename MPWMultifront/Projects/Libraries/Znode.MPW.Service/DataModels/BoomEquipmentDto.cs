﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{
    public class BoomEquipmentDto
    {
        public BoomEquipmentDto() => this.Categories = new List<BoomEquipmentCategoryDto>();
        [JsonProperty("EquipmentID")]
        public Guid EquipmentId { get; set; }
        public string UnitNumber { get; set; }
        public string UnitType { get; set; }
        [JsonProperty("EquipmentTypeID")]
        public Guid EquipmentTypeId { get; set; }
        public int UnitTypeNum { get; set; }
        public string UnitDescription { get; set; }
        public int AssetNumber { get; set; }
        public int? CostCenter { get; set; }
        public bool Active { get; set; }
        [JsonProperty("AssociatedMPWFacilityID")]
        public Guid? AssociatedMpwFacilityId { get; set; }
        [JsonProperty("AssociatedMPWFacilityName")]
        public string AssociatedMpwFacilityName { get; set; }
        [JsonProperty("CurrentLocationID")]
        public Guid? CurrentLocationId { get; set; }
        [JsonProperty("HasBeenToIL")]
        public bool HasBeenToIl { get; set; }
        public DateTime? MfgDate { get; set; }
        [JsonProperty("CanGoToIL")]
        public bool CanGoToIl { get; set; }
        public List<BoomEquipmentCategoryDto> Categories { get; set; }
        public int? DataCollectionLocationId { get; set; }
        [JsonProperty("DataCollectionEquipmentTypeID")]
        public int DataCollectionEquipmentTypeId { get; set; }
        public int? DataCollectionRequestStatus { get; set; }
        public string DataCollectionEquipmentTypeIdentifier { get; set; }
    }
}
