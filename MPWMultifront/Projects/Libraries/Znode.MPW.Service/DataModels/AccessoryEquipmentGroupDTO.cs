﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{

    [JsonObject]
    public class AccessoryEquipmentGroupDTO
    {
        public AccessoryEquipmentGroupDTO()
        {
            AccessoryEquipmentListDTO = new List<AccessoryEquipmentListDTO>();
        }

        /// <summary>
        /// Used for Mobile as an identifier.
        /// </summary>
        [JsonProperty]
        public int MobileAccessoryEquipmentGroupID { get; set; }

        [JsonProperty]
        public Guid CustomerFacilityID { get; set; }

        [JsonProperty]
        public Guid ScheduledItemID { get; set; }

        [JsonProperty]
        public Guid AccessoryEquipmentGroupID { get; set; }

        [JsonProperty]
        public string SpecialEquipment { get; set; } 

        [JsonProperty]
        public string ConnectionSpecs { get; set; }

        [JsonProperty]
        public List<AccessoryEquipmentListDTO> AccessoryEquipmentListDTO { get; set; }
        /// <summary>
        /// Currently just used for Work Order Accessory Equipment Groups
        /// </summary>

        [JsonProperty]
        public bool IsDelivery { get; set; }
    }
}
