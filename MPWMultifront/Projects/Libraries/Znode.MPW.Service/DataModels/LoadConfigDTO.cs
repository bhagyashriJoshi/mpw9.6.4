﻿using System;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{
    [JsonObject]
    public class LoadConfigDTO
    {
        [JsonProperty]
        public Guid LoadID { get; set; }

        [JsonProperty]
        public Guid CustomerFacilityID { get; set; }

        [JsonProperty]
        public Guid LoadTypeID { get; set; }

        [JsonProperty]
        public string LoadTypeDescription { get; set; }

        [JsonProperty]
        public int FilterCount { get; set; }

        [JsonProperty]
        public int WeakCationCount { get; set; }

        [JsonProperty]
        public int CationCount { get; set; }

        [JsonProperty]
        public int WeakCount { get; set; }

        [JsonProperty]
        public int AnionCount { get; set; }

        [JsonProperty]
        public int MixBedCount { get; set; }

        [JsonProperty]
        public bool IsDefault { get; set; }

        [JsonProperty]
        public string Grains { get; set; }

        [JsonProperty]
        public int MixedBed1_2Count { get; set; }

        [JsonProperty]
        public int MixedBed3_6Count { get; set; }

        [JsonProperty]
        public int MixedBed30Count { get; set; }

        [JsonProperty]
        public int MixedBed90Count { get; set; }

        [JsonProperty]
        public int Cation90Count { get; set; }

        [JsonProperty]
        public string LoadName { get; set; }

        [JsonProperty]
        public bool IsActive { get; set; }

        [JsonProperty]
        public int MixedBedHF30Count { get; set; }

        [JsonProperty]
        public int? Co2 { get; set; }
    }
}
