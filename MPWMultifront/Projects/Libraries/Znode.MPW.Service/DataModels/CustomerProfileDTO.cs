﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{
    [JsonObject]
    public class CustomerProfileDTO
    {
        [JsonProperty]
        public List<CustomerDetailsDTO> CustomerFacilities { get; set; }

        [JsonProperty]
        public int JDENumber { get; set; }
    }
}
