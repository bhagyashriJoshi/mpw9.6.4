﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{
    public class BoomEquipmentCategoryOptionDto
    {
        [JsonProperty("DataCategoryID")]
        public Guid DataCategoryId { get; set; }
        public string EquipmentDataCategoryOption { get; set; }
        [JsonProperty("EquipmentDataCategoryOptionID")]
        public Guid EquipmentDataCategoryOptionId { get; set; }
        public bool IsActive { get; set; }
    }
}
