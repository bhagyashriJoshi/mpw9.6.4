﻿namespace Znode.MPW.Service.DataModels
{
    /// <summary>
    ///     Work order billable line items
    /// </summary>
    public class CustomerWorkorderLineItem
    {
        public decimal? Units { get; set; }
        public string UOM { get; set; }
        public int InvoiceNumber { get; set; }
        public string ProductName { get; set; }
    }
}
