﻿namespace Znode.MPW.Service.DataModels
{
    /// <summary>
    ///     Description lines displayed at the top of the work order
    /// </summary>
    public class CustomerWorkorderDescLine
    {
        public string Description { get; set; }
    }
}
