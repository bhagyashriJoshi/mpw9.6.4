﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{
    public class BoomEquipmentCategoryDto
    {
        [JsonProperty("DataCategoryID")]
        public Guid DataCategoryId { get; set; }
        public string DataCategory { get; set; }
        [JsonProperty("DataCategoryParentID")]
        public Guid DataCategoryParentId { get; set; }
        public string DataCategoryParent { get; set; }
        public string CategoryType { get; set; }
        public Guid SelectedValue { get; set; }
        public string SelectedText { get; set; }
        public string User { get; set; }
        public List<BoomEquipmentCategoryOptionDto> Options { get; set; }
        [JsonProperty("DataCategoryTypeID")]
        public Guid DataCategoryTypeId { get; set; }
        public bool IsActive { get; set; }
        public int SortOrder { get; set; }
        public bool OnlyNameParentEditable { get; set; }
    }
}
