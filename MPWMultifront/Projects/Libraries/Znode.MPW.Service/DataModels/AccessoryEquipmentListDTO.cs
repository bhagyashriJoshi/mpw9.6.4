﻿using System;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{
    [JsonObject]
    public class AccessoryEquipmentListDTO
    {
        public AccessoryEquipmentListDTO()
        {

        }

        /// <summary>
        /// Used for Mobile as an identifier.
        /// </summary>
        [JsonProperty]
        public int MobileAccessoryEquipmentListID { get; set; }

        [JsonProperty]
        public Guid AccessoryEquipmentGroupID { get; set; }

        [JsonProperty]
        public Guid AccessoryEquipmentDetailID { get; set; }
        
        [JsonProperty]
        public int Quantity { get; set; }
        [JsonProperty]
        public bool QuantityAllowed { get; set; }

        [JsonProperty]
        public string AccessoryEquipmentName { get; set; }
        
        [JsonProperty]
        public Guid AccessoryEquipmentTypeID { get; set; }
        [JsonProperty]
        public string AccessoryEquipmentTypeName { get; set; }
        [JsonProperty]
        public int AccessoryEquipmentTypeDisplayOrder { get; set; }
        [JsonProperty]
        public int AccessoryEquipmentDetailDisplayOrder { get; set; }
        [JsonProperty]
        public bool IsActive { get; set; }
        
        public string QuantityAsString
        {
            get
            {
                return QuantityAllowed ? Quantity.ToString() : Quantity == 0 ? "NO" : "YES";
            }
        }

        public string QuantityAllowedAsString
        {
            get
            {
                return QuantityAllowed ? "YES" : "NO";
            }
        }
    }
}
