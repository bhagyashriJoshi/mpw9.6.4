﻿using System;

namespace Znode.MPW.Service.DataModels
{
    public class ScheduledJobDTO
    {
        public Guid InitiatorID { get; set; }
        public Guid CustomerFacilityID { get; set; }
        public string CustomerFacilityName { get; set; }
        public Guid? WorkOrderID { get; set; }
        public Guid? EquipmentID { get; set; }
        public string UnitNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderNumber {get; set;}
        public string LoadName { get; set; }
        public string Status { get; set; }
        public int? DCLocation { get; set; }
        public string CustomerNotes { get; set; }
        public bool ReleaseSwapAvailable { get; set; }
        public string OrderType { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string ReleaseBy { get; set; }
    }
}
