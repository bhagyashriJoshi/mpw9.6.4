﻿using System;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{
    [JsonObject]
    public class CustomerOrderDTO
    {
        [JsonProperty] public int JDENumber { get; set; }

        [JsonProperty] public string PONumber { get; set; }

        [JsonProperty] public Guid CustomerFacilityID { get; set; }

        [JsonProperty] public string CustomerFacilityName { get; set; }

        [JsonProperty] public Guid LoadID { get; set; }

        [JsonProperty] public DateTime? RequestedDate { get; set; }

        [JsonProperty] public int DeliveryLevel { get; set; }

        [JsonProperty] public string HoursAvailable { get; set; }

        [JsonProperty] public string CustomerNotes { get; set; }

        [JsonProperty] public int? ZnodeID { get; set; }

        [JsonProperty] public string ZnodeUserFirstName { get; set; }

        [JsonProperty] public string ZnodeUserLastName { get; set; }

        [JsonProperty] public string ZnodeUserPhoneNumber { get; set; }

        [JsonProperty] public string ZnodeUserEmail { get; set; }

        [JsonProperty] public string RequestorFirstName { get; set; }

        [JsonProperty] public string RequestorLastName { get; set; }

        [JsonProperty] public string RequestorPhoneNumber { get; set; }

        [JsonProperty] public string RequestorEmail { get; set; }

        [JsonProperty] public string InitiatorType { get; set; }

        [JsonProperty] public Guid? EquipmentID { get; set; }

        [JsonProperty] public Guid? WorkorderID { get; set; }

        [JsonProperty] public string ReleaseNotes { get; set; }

        [JsonProperty] public DateTime? ReleaseDate { get; set; }

        [JsonProperty] public int Quantity { get; set; }
    }
}