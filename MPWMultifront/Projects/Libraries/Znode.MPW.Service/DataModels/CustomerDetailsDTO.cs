﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Znode.MPW.Service.DataModels
{
    [JsonObject]
    public class CustomerDetailsDTO
    {
        [JsonProperty]
        public string CustomerFacilityName { get; set; }
        [JsonProperty]
        public Guid CustomerFacilityID { get; set; }
        [JsonProperty]
        public List<LoadConfigDTO> DILoads { get; set; }
        [JsonProperty]
        public AccessoryEquipmentGroupDTO AccessoryEquipment { get; set; }
        [JsonProperty]
        public int CurrentTrailerCount { get; set; }
    }
}
