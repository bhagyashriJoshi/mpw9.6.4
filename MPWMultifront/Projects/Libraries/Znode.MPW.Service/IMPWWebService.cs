﻿using System;
using System.Collections.Generic;
using Znode.MPW.Service.DataModels;

namespace Znode.MPW.Service
{
    public interface IMPWWebService
    {
        bool GetAuthenticationToken();
        CustomerProfileDTO GetCustomerProfile(string jdeNumber);
        List<BoomEquipmentDto> GetCurrentEnabledEquipmentOnSite(Guid customerFacilityId);

        List<ScheduledJobDTO> GetScheduledJobs(Guid customerFacilityID, DateTime? startDate,
            DateTime? endDate);

        List<CustomerProfileDTO> GetCustomerProfilesByList(string[] jdeNumbers);
        CustomerWorkorder GetWorkOrderByID(string workOrderId);
        int CreateOrder(List<CustomerOrderDTO> orders);
    }
}