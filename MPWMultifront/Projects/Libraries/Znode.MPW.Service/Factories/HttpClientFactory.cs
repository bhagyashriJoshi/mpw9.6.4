﻿using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using MPW.Common.Cryptography;

namespace Znode.MPW.Service.Factories
{
    public sealed class HttpClientFactory : IHttpClientFactory
    {
        private readonly string _url = ConfigurationManager.AppSettings["MPWService_Url"];
        private readonly string _requestTimeOut = ConfigurationManager.AppSettings["MPWService_RequestTimeOut"];
        private readonly string _username = ConfigurationManager.AppSettings["MPWService_Username"];
        private readonly string _password = ConfigurationManager.AppSettings["MPWService_Password"];
        private readonly string _clientKey = ConfigurationManager.AppSettings["MPWService_ClientKey"];
        private readonly string _clientVersion = ConfigurationManager.AppSettings["MPWService_ClientVersion"];
        
        private string _securityToken;
        
        private static HttpClient _client;

        public HttpClient GetClient()
        {
            try
            {
                if (_client == null)
                {
                    _client = new HttpClient();
                    SetupClientDefaults(_client);
                }
                SetAuthorization(_client);
                return _client;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public void RefreshClient()
        {
            _client.Dispose();
            _client = null;
        }

        public void SetSecurityToken(string token)
        {
            _securityToken = token;
        }

        public string GetCredentials()
        {
            string userData = _username + ":" + _password;
            string encryptedUserData = Crypto.Encrypt_Aes(userData);
            return encryptedUserData;
        }

        public bool IsAuthorized()
        {
            return !string.IsNullOrWhiteSpace(_securityToken);
        }
        
        private void SetAuthorization(HttpClient client)
        {
            if (!IsAuthorized())
            {
                return;
            }
            
            string securityToken = _securityToken;
            if (client.DefaultRequestHeaders.Any(x => x.Key == "Authorization"))
            {
                client.DefaultRequestHeaders.Remove("Authorization");
            }
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", securityToken);
        }

        private void SetupClientDefaults(HttpClient client)
        {
            double.TryParse(_requestTimeOut, out double timeOut);
            client.Timeout = TimeSpan.FromSeconds(timeOut);
            client.BaseAddress = new Uri(_url);

            string clientHeader = Crypto.Encrypt_Aes($"{_clientKey}:{_clientVersion}");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Client", clientHeader);
        }
    }
}