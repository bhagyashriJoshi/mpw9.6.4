﻿using System.Net.Http;

namespace Znode.MPW.Service.Factories
{
    public interface IHttpClientFactory
    {
        HttpClient GetClient();
        void SetSecurityToken(string token);
        string GetCredentials();
        bool IsAuthorized();
    }
}