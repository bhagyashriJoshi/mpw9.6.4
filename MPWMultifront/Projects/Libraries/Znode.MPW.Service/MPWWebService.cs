﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Znode.MPW.Service.DataModels;
using Znode.MPW.Service.Factories;

namespace Znode.MPW.Service
{
    public class MPWWebService : IMPWWebService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public MPWWebService(IHttpClientFactory httpClientFactory)
        {
            try
            {
                _httpClientFactory = httpClientFactory;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool GetAuthenticationToken()
        {
            bool retVal = false;
            try
            {
                if (_httpClientFactory.IsAuthorized())
                {
                    return false;
                }

                //User data
                var value = _httpClientFactory.GetCredentials();

                var response = _httpClientFactory.GetClient().GetAsync($"GetAuthenticationToken/{value}").Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();

                    string token = JsonConvert.DeserializeObject<string>(jsonString.Result);

                    _httpClientFactory.SetSecurityToken(token);
                    retVal = !string.IsNullOrWhiteSpace(token);
                }
                else
                {
                    Console.WriteLine($"{response.StatusCode} - {response.ReasonPhrase}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetAuthenticationToken Exception: " + ex.Message);
            }

            return retVal;
        }

        public List<BoomEquipmentDto> GetCurrentEnabledEquipmentOnSite(Guid customerFacilityId)
        {
            try
            {
                var response = _httpClientFactory.GetClient()
                    .GetAsync(
                        $"GetCurrentEnabledEquipmentOnSite/{customerFacilityId}").Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    if (!string.IsNullOrEmpty(jsonString.Result))
                    {
                        return JsonConvert.DeserializeObject<List<BoomEquipmentDto>>(jsonString.Result);
                    }
                }
                else
                {
                    Console.WriteLine($"{response.StatusCode} - {response.ReasonPhrase}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetCurrentEnabledEquipmentOnSite Exception: " + ex.Message);
            }

            return new List<BoomEquipmentDto>();
        }

        public CustomerProfileDTO GetCustomerProfile(string jdeNumber)
        {
            try
            {
                var response = _httpClientFactory.GetClient().GetAsync($"GetCustomerProfile/{jdeNumber}").Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();

                    if (!string.IsNullOrEmpty(jsonString.Result))
                    {
                        return JsonConvert.DeserializeObject<CustomerProfileDTO>(jsonString.Result);
                    }

                    return new CustomerProfileDTO();
                }

                Console.WriteLine($"{response.StatusCode} - {response.ReasonPhrase}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetCustomerProfile Exception: " + ex.Message);
            }

            return null;
        }

        public List<CustomerProfileDTO> GetCustomerProfilesByList(string[] jdeNumbers)
        {
            try
            {
                string jdeNumbersString = string.Join(",", jdeNumbers);
                var response = _httpClientFactory.GetClient()
                    .GetAsync($"GetCustomerProfilesByList/{jdeNumbersString}").Result;

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();

                    if (!string.IsNullOrEmpty(jsonString.Result))
                    {
                        return JsonConvert.DeserializeObject<List<CustomerProfileDTO>>(jsonString.Result);
                    }
                }

                Console.WriteLine($"{response.StatusCode} - {response.ReasonPhrase}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetCustomerProfilesByList Exception: " + ex.Message);
            }

            return null;
        }

        public CustomerWorkorder GetWorkOrderByID(string workOrderId)
        {
            try
            {
                Guid customerWorkOrderId = new Guid(workOrderId);
                var response = _httpClientFactory.GetClient()
                    .GetAsync($"GetWorkorderByID/{customerWorkOrderId}").Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();

                    if (!string.IsNullOrEmpty(jsonString.Result))
                    {
                        return JsonConvert.DeserializeObject<CustomerWorkorder>(jsonString.Result);
                    }
                }

                Console.WriteLine($"{response.StatusCode} - {response.ReasonPhrase}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetWorkOrderByID Exception: " + ex.Message);
            }

            return new CustomerWorkorder();
        }

        public List<ScheduledJobDTO> GetScheduledJobs(Guid customerFacilityId, DateTime? startDate,
            DateTime? endDate)
        {
            try
            {
                DateTime jobStartDate = new DateTime(), jobEndDate = new DateTime();
                if (!Equals(startDate, null))
                {
                    jobStartDate = new DateTime(startDate.Value.Year, startDate.Value.Month, startDate.Value.Day,
                        startDate.Value.Hour, startDate.Value.Minute, startDate.Value.Second, DateTimeKind.Utc);
                }

                if (!Equals(endDate, null))
                {
                    jobEndDate = new DateTime(endDate.Value.Year, endDate.Value.Month, endDate.Value.Day,
                        endDate.Value.Hour, endDate.Value.Minute, endDate.Value.Second, DateTimeKind.Utc);
                }

                string userJobStartDate = Equals(startDate, null) ? null : jobStartDate.ToString("yyyyMMddHHmmss");
                string userJobEndDate = Equals(endDate, null) ? null : jobEndDate.ToString("yyyyMMddHHmmss");

                var response = _httpClientFactory.GetClient()
                    .GetAsync(
                        $"GetJobsForCustomer/{customerFacilityId.ToString()}_{userJobStartDate}_{userJobEndDate}")
                    .Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    if (!string.IsNullOrEmpty(jsonString.Result))
                    {
                        return JsonConvert.DeserializeObject<List<ScheduledJobDTO>>(jsonString.Result);
                    }
                }

                Console.WriteLine($"{response.StatusCode} - {response.ReasonPhrase}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetScheduledJobs Exception: " + ex.Message);
            }

            return null;
        }

        public int CreateOrder(List<CustomerOrderDTO> orders)
        {
            try
            {
                JsonSerializerSettings microsoftDateFormatSettings = new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                };
                string serializedOrders = JsonConvert.SerializeObject(orders, microsoftDateFormatSettings);
                var httpcontent = new StringContent(serializedOrders, Encoding.UTF8, "application/json");
                var response = _httpClientFactory.GetClient()
                    .PutAsync("SaveCustomerRequestedOrders", httpcontent)
                    .Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    if (!string.IsNullOrEmpty(jsonString.Result))
                    {
                        List<int> ids = JsonConvert.DeserializeObject<List<int>>(jsonString.Result);
                        return ids.FirstOrDefault();
                    }
                }
                Console.WriteLine($"{response.StatusCode} - {response.ReasonPhrase}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("CreateOrder Exception: " + ex.Message);
            }
            return 0;
        }
    }
}