﻿using System;
using System.IO;
using System.Net;
using Znode.Engine.Api.Models;

namespace Znode.Libraries.Hangfire
{
    public class VoucherEmailHelper : BaseScheduler,ISchedulerProviders
    {
        #region MyRegion
        private const string SchedulerName = "VoucherReminderEmail";
        private const string AuthorizationHeader = "Authorization";
        private const string UserHeader = "Znode-UserId";
        private const string TokenHeader = "Token";
        private string TokenValue = string.Empty;
        #endregion

        public void InvokeMethod(ERPTaskSchedulerModel model)
        {
            if (!string.IsNullOrEmpty(model.ExeParameters))
            {
                var args = model.ExeParameters.Split(',');
                TokenValue = args[4];
                // args[7] contains request timeout value.
                if (args.Length > 7 && !string.IsNullOrEmpty(args[7]))
                    base.requesttimeout = int.Parse(args[7]);

                CallVoucherEmailAPI(args[1], args[2], args[3]);
            }
        }

        //To call Voucher reminder email API.
        private void CallVoucherEmailAPI( string apiDomainURL, string userId, string token)
        {
            string requestPath = $"{apiDomainURL}/giftcard/sendvoucherexpirationreminderemail";
            string jsonString = string.Empty;
            LogMessage(requestPath, SchedulerName);

            try
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                    request.Method = "GET";
                    request.ContentType = "application/json";
                    request.Headers.Add($"{ UserHeader }: { userId }");
                    request.Headers.Add($"{ AuthorizationHeader }: Basic { token }");

                    if (!string.IsNullOrEmpty(TokenValue) && TokenValue != "0")
                        request.Headers.Add($"{ TokenHeader }: { TokenValue }");

                    request.Timeout = base.requesttimeout;
                    LogMessage("Email schedular method Called and Set request parameter.", SchedulerName);
                    using (HttpWebResponse responce = (HttpWebResponse)request.GetResponse())
                    {
                        Stream datastream = responce.GetResponseStream();
                        LogMessage("Got Response Stream.", SchedulerName);
                        StreamReader reader = new StreamReader(datastream);
                        LogMessage("read Response Stream.", SchedulerName);
                        jsonString = reader.ReadToEnd();
                        reader.Close();
                        datastream.Close();
                        LogMessage("API Call Successfully.", SchedulerName);
                        //Call Send Scheduler Activity Log Method with "No Error" and scheduler status as true
                        LogMessage("Sending Scheduler Activity Log.", SchedulerName);

                    }
                }
                catch (WebException webException)
                {
                    if (CheckTokenIsInvalid(webException))
                    {
                        TokenValue = GetToken(apiDomainURL, token);
                        CallVoucherEmailAPI(apiDomainURL, userId, token);
                    }
                    else
                        LogMessage($"Email schedular - Failed: {webException.Message} , StackTrace: {webException.StackTrace}", SchedulerName);
                }
            }
            catch (Exception ex)
            {
                LogMessage($"Email schedular - Failed: {ex.Message} , StackTrace: {ex.StackTrace}", SchedulerName);
            }
        }
    }
}

