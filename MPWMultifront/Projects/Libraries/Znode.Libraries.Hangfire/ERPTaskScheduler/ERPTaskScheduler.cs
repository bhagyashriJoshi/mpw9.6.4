﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Znode.Engine.Api.Models;

namespace Znode.Libraries.Hangfire
{
    public class ERPTaskScheduler : BaseScheduler, ISchedulerProviders
    {
        #region Public Methods
        public string erpExeLogFilePath { get; } = string.Empty;
        private const string SchedulerName = "ERPTaskScheduler";
        private const string UserHeader = "Znode-UserId";
        private const string AuthorizationHeader = "Authorization";
        private const string TokenHeader = "Token";
        public void InvokeMethod(ERPTaskSchedulerModel model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.ExeParameters))
                {
                    var args = model.ExeParameters.Split(' ');
                    string TokenBasedAuthTokenValue = string.Empty;
                    string AuthorizationHeaderValue = string.Empty;

                    LogMessage("Start to run EXE for " + args[4] + "Scheduler.", SchedulerName);
                    ERPTaskScheduler program = new ERPTaskScheduler();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Please wait....");
                    LogMessage("SyncCallApi method Calling.", SchedulerName);
                    // args[7] contains token value.
                    if (args.Length > 7 && !string.IsNullOrEmpty(args[7]) && args[7] != "0")
                    {
                        TokenBasedAuthTokenValue = args[7];
                    }
                    // args[8] contains request timeout value.
                    if (args.Length > 8 && !string.IsNullOrEmpty(args[8]))
                    {
                        base.requesttimeout = int.Parse(args[8]);
                    }
                    program.SyncCallApi(args);
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                LogMessage($"Failed : {ex.Message}, stack: {ex.StackTrace}", SchedulerName);
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Connect to ERPTaskScheduler API 
        /// </summary>
        /// <param name="args">Parameter with task scheduler</param>
        /// <returns>Return responce by API controller</returns>
        private string SyncCallApi(string[] arguments)
        {
            string jsonString = string.Empty;
            string requestPath = string.Concat(arguments[3], "/ERPTaskScheduler/TriggerSchedulerTask/", arguments[1]);
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-url-encoded";
                //If logged-in user id received in argument (It's 6th argument), It'll be used to invoke the call.
                if (arguments.Length > 5)
                    request.Headers.Add($"{ UserHeader }: { arguments[5] }");

                if (arguments.Length > 6)
                {
                    request.Headers.Add($"{ AuthorizationHeader }: Basic { arguments[6] }");
                }

                if (arguments.Length > 7 && !string.IsNullOrEmpty(arguments[7]) && arguments[7] != "0")
                {
                    request.Headers.Add($"{TokenHeader}: {arguments[7]}");
                }
                if (arguments.Length > 8 && !string.IsNullOrEmpty(arguments[8]))
                {
                    base.requesttimeout = int.Parse(arguments[8]);
                }
                request.Timeout = base.requesttimeout;
                LogMessage("SyncCallApi method Called and Set request parameter.", SchedulerName);
                using (HttpWebResponse responce = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = responce.GetResponseStream();
                    LogMessage("Got Response Stream.", SchedulerName);
                    StreamReader reader = new StreamReader(datastream);
                    LogMessage("read Response Stream.", SchedulerName);
                    jsonString = reader.ReadToEnd();
                    reader.Close();
                    datastream.Close();
                    LogMessage("API Call Successfully.", SchedulerName);
                    //Call Send Scheduler Activity Log Method with "No Error" and scheduler status as true
                    LogMessage("Sending Scheduler Activity Log.", SchedulerName);
                    SendSchedulerActivityLog("No Error", true, arguments[3], arguments[2], arguments[1], arguments[6], (arguments.Length > 7) ? arguments[7] : "");
                }
            }
            catch (WebException webException)
            {
                if (CheckTokenIsInvalid(webException))
                {
                    // args[6] contains authorization header value.
                    // args[3] contains domain url.
                    arguments[7] = GetToken(arguments[3], arguments[6]);
                    SyncCallApi(arguments);
                }
                else
                {
                    //Call Send Scheduler Activity Log Method with "Error" and scheduler status as false
                    SendSchedulerActivityLog(webException.Message, false, arguments[3], arguments[2], arguments[1], arguments[6], (arguments.Length > 7) ? arguments[7] : "");
                    LogMessage($"Failed : {webException.Message}, stack: {webException.StackTrace}", SchedulerName);
                }
            }
            catch (Exception ex)
            {
                //Call Send Scheduler Activity Log Method with "Error" and scheduler status as false
                SendSchedulerActivityLog(ex.Message, false, arguments[3], arguments[2], arguments[1], arguments[6], (arguments.Length > 7) ? arguments[7] : "");
                LogMessage($"Failed : {ex.Message}, stack: {ex.StackTrace}", SchedulerName);
            }
            return jsonString;
        }

        private bool SendSchedulerActivityLog(string errorMessage, bool schedulerStatus, string domailUrl, string portalId, string eRPTaskSchedulerId, string AuthorizationHeaderValue = "", string TokenBasedAuthTokenValue = "")
        {
            //Create static json string
            string data = string.Concat("{", '"', "ErrorMessage", '"', ":", '"', errorMessage, '"', ',', '"', "SchedulerStatus", '"', ":", '"', schedulerStatus, '"', ',', '"', "PortalId", '"', ":", '"', portalId, '"', ',', '"', "ERPTaskSchedulerId", '"', ":", '"', eRPTaskSchedulerId, '"', "}");
            //Create endpoint
            string endpoint = string.Concat(domailUrl, "/TouchPointConfiguration/SendSchedulerActivityLog");
            return PostResourceToEndpoint(endpoint, data, domailUrl, AuthorizationHeaderValue, TokenBasedAuthTokenValue);
        }

        private bool PostResourceToEndpoint(string endpoint, string data, string domailUrl = "", string AuthorizationHeaderValue = "", string TokenBasedAuthTokenValue = "")
        {
            LogMessage("Posting Resource To Endpoint - " + endpoint, SchedulerName);
            var dataBytes = Encoding.UTF8.GetBytes(data);
            var req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "POST";
            req.ContentType = "application/json";
            req.ContentLength = dataBytes.Length;
            req.Headers.Add($"Authorization: {"Basic " + AuthorizationHeaderValue}");
            if (!string.IsNullOrEmpty(TokenBasedAuthTokenValue))
                req.Headers.Add($"Token: {TokenBasedAuthTokenValue}");

            using (var reqStream = req.GetRequestStream())
            {
                reqStream.Write(dataBytes, 0, dataBytes.Length);
            }
            LogMessage("Read request stream for Email.", SchedulerName);

            try
            {
                if (((HttpWebResponse)req.GetResponse()).StatusCode == HttpStatusCode.OK)
                {
                    LogMessage("Email Sent Successfully.", SchedulerName);
                    return true;
                }
                else
                {
                    LogMessage("Failed to Sent Email.", SchedulerName);
                    return false;
                }
            }
            catch (WebException webException)
            {
                if (CheckTokenIsInvalid(webException))
                {
                    TokenBasedAuthTokenValue = GetToken(domailUrl, AuthorizationHeaderValue);
                    PostResourceToEndpoint(endpoint, data, domailUrl, AuthorizationHeaderValue, TokenBasedAuthTokenValue);
                }
                else
                    LogMessage("Failed to Sent Email.", SchedulerName);
                return false;
            }
            catch (Exception ex)
            {
                LogMessage("Failed to Sent Email.", SchedulerName);
                return false;
            }
        }

        #endregion
    }
}
