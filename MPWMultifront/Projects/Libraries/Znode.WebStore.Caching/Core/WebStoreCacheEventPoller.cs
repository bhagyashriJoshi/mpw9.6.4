﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Caching;
using Znode.WebStore.Caching.Evictors;

namespace Znode.WebStore.Caching.Core
{
    public class WebStoreCacheEventPoller : BaseCacheEventPoller
    {
        public WebStoreCacheEventPoller(int delayTime) : base(delayTime)
        {

        }
    }
}
