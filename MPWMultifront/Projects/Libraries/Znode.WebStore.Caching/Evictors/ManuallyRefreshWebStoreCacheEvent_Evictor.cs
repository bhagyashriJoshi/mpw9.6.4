﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Caching;
using Znode.Libraries.Caching.Events;
using Znode.Libraries.ECommerce.Utilities;
using Znode.WebStore.Caching.Core;

namespace Znode.WebStore.Caching.Evictors
{
    internal class ManuallyRefreshWebStoreCacheEvent_Evictor : BaseWebStoreEvictor<ManuallyRefreshWebStoreCacheEvent>
    {
        protected override void Setup(ManuallyRefreshWebStoreCacheEvent cacheHtmlCacheEvent)
        {

        }

        protected override void EvictNonDictionaryCacheData(ManuallyRefreshWebStoreCacheEvent cacheHtmlCacheEvent)
        {
            ClearHtmlCache();
        }

        protected override List<string> EvictSpecificDictionaryCacheKeys(ManuallyRefreshWebStoreCacheEvent cacheHtmlCacheEvent)
        {
            return new List<string>();
        }

        protected override bool IsDictionaryItemStale(ManuallyRefreshWebStoreCacheEvent cacheHtmlCacheEvent, string key)
        {
            return true;
        }

        protected override void Teardown(ManuallyRefreshWebStoreCacheEvent cacheHtmlCacheEvent)
        {

        }
    }
}