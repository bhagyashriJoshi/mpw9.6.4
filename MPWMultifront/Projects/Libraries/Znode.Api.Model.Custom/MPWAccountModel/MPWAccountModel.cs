﻿using Znode.Engine.Api.Models;

namespace Znode.Api.Model.Custom.MPWAccountModel
{
    public class MPWAccountModel : BaseModel
    {
        public string AccountName { get; set; }
        public string StoreName { get; set; }
        public int? PortalId { get; set; }
        public int AccountId { get; set; }
        public string ExternalId { get; set; }
        public string AccountCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string DITrailerEnabled { get; set; }
        public string BoomEnabled { get; set; }
        public string MobileEquipmentEnabled { get; set; }
        public string ConditionalFormattingEnabled { get; set; }
    }
}
