﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Api.Model.Custom.MPWAccountModel
{
    public class MPWUserAccountListModel : BaseModel
    {
        public List<MPWAccountModel> AccountDetails { get; set; }

        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int? PortalId { get; set; }

    }
}
