﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Model.Custom.MPWAccountModel;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Api.Model.Custom.MPWUserAccountListResponse
{
    public class MPWUserAccountListResponse : BaseResponse
    {
        public MPWUserAccountListModel UserAccountDetails { get; set; }

    }
}
