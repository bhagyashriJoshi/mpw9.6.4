﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.Caching.Events
{
    public class StaleWebStoreKeyEvent : BaseCacheEvent
    {
        public string Key { get; set; }
        public int[] PortalIds { get; set; }
    }
}
