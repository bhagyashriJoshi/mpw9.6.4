﻿/*Water Direct*/
$(document).ready(function () {
    console.log("water-direct ready!");

    $('#btnRefreshData').on('click',
        function () {
            window.location = window.location.href;
        });

    $(".userAssignedAccountsList").on("change",
        function () {
            var selectedVal = $(".userAssignedAccountsList :selected").val();
            InsertUrlParam("account", selectedVal, true);
        });
    $(".boomEquipmentTypesList").on("change",
        function () {
            var selectedVal = $(".boomEquipmentTypesList :selected").val();
            var url = '/WaterDirect/BoomEquipment?account=' + $(".userAssignedAccountsList :selected").val();
            url = url + '&type=' + selectedVal;
            window.location = url;
        });
    $('#pageSize').on("change",
        function () {
            var selectedVal = $('#pageSize :selected').val();
            InsertUrlParam("size", selectedVal);
            modelObj.PageSize = selectedVal;
            modelObj.PageIndex = 0;
            PopulateFilteredOrderHistory();
        });
    $('#btnRefreshOrderHistory').on("click", function () {
        PopulateFilteredOrderHistory(true);
    })
    $('#btnOrderTrailer').on("click", GetOrderPage)
    $('#btnClearFilters').on("click", ClearFilters);
    $('#startDateFilter').on("changeDate", FilterByOrderDate);
    $('#endDateFilter').on("changeDate", FilterByOrderDate);
    BindOrderHistoryEvents();

    $('#spinnerTrailerQuantity_plus').on("click", function () {
        var i = $("#spinnerTrailerQuantity").val();
        i++;
        $('#spinnerTrailerQuantity').val(i);
    });
    $('#spinnerTrailerQuantity_minus').on("click", function () {
        var i = $("#spinnerTrailerQuantity").val();
        i--;
        $('#spinnerTrailerQuantity').val(i);
    });
    $('#radioSwapYes > input').on("change", function () {
        $('#spinnerTrailerQuantityGroup').hide();
    });
    $('#radioSwapNo > input').on("change", function () {
        $('#spinnerTrailerQuantityGroup').show();
    })
    $('#radio24Hour').on("change", function () {
        $('#deliveryDatePicker').datepicker('destroy').prop("disabled", true);
    })
    $('#radioSelectDate').on("change", function () {
        $('#deliveryDatePicker').datepicker({
            todayHighlight: true,

        }).prop("disabled", false);
    })
    $('#btnOrderSubmit').on("click", function () {
        if ($("#OrderForm").valid()) {
            $('#OrderForm').submit();
        } else {
            return false;
        }
    })
    $('#OrderForm').on("submit", function () {
        
    })
});

let modelObj;
var timeout = 0;
var oneSecondTimer;

function Init(model) {
    modelObj = model;
}

function InsertUrlParam(key, value, refresh = false) {
    if (history.pushState) {
        let searchParams = new URLSearchParams(window.location.search);
        searchParams.set(key, value);

        if (refresh) {
            window.location.search = searchParams.toString();
        } else {
            let newUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + searchParams.toString();
            window.history.pushState({path: newUrl}, '', newUrl);
        }
    }
}

function removeUrlParam(key, refresh = false) {
    let searchParams = new URLSearchParams(window.location.search);
    searchParams.delete(key);

    if (refresh) {
        window.location.search = searchParams.toString();
    } else {
        let newUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + searchParams.toString();
        window.history.pushState({path: newUrl}, '', newUrl);
    }
}

function GetWaterDirectNavigation() {
    return {
        SelectedUserAssignedAccount: modelObj.WaterDirectNavigation.SelectedUserAssignedAccount,
        WaterDirectNavPermissions: {
            CanViewDiEquipment: modelObj.WaterDirectNavigation.WaterDirectNavPermissions.CanViewDiEquipment,
            CanViewMobileEquipment: modelObj.WaterDirectNavigation.WaterDirectNavPermissions.CanViewMobileEquipment,
            CanViewBoomEquipment: modelObj.WaterDirectNavigation.WaterDirectNavPermissions.CanViewBoomEquipment,
        }
    };
}

function BindOrderHistoryEvents() {
    $('.viewWorkorder').on("click", function () {
        var workorderId = $(this).data("workorderid");
        GetViewWorkorderPage(workorderId);
    })
    $('.pagerNumber a').on("click", GetPage);
    $('.pagerNext a').on("click", GetPage);
    $('.pagerPrevious a').on("click", GetPage);
}

function GetPage(e) {
    e.preventDefault();
    var selectedVal = $(this).data("pageindex");
    InsertUrlParam("page", selectedVal);
    modelObj.PageIndex = selectedVal;
    PopulateFilteredOrderHistory();
}

function FilterByOrderDate(e) {
    var startDate = $('#startDateFilter').val();
    var endDate = $('#endDateFilter').val();
    if (startDate && endDate) {
        InsertUrlParam("start", startDate);
        InsertUrlParam("end", endDate);
        modelObj.StartDate = startDate;
        modelObj.EndDate = endDate;
        PopulateFilteredOrderHistory();
    }
}

function ClearFilters(e) {
    $('#startDateFilter').datepicker('update', '');
    $('#endDateFilter').datepicker('update', '');

    removeUrlParam("start");
    removeUrlParam("end");

    modelObj.StartDate = null;
    modelObj.EndDate = null;

    PopulateFilteredOrderHistory();
}

function PopulateFilteredOrderHistory(refreshCache = false) {
    var pageSize = modelObj.PageSize
    var pageIndex = modelObj.PageIndex
    var startDate = modelObj.StartDate
    var endDate = modelObj.EndDate

    var model = {
        PageSize: pageSize,
        PageIndex: pageIndex,
        StartDate: startDate,
        EndDate: endDate,
        WaterDirectNavigation: GetWaterDirectNavigation(),
        RefreshCache: refreshCache
    }
    $.ajax({
        url: "/WaterDirect/GetDiOrderHistory",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(model),
        success: function (result) {
            $("#divOrderHistory").html(result);
            BindOrderHistoryEvents();
        },
        failed: function (result) {
            console.log(result);
        }
    });
}

function GetViewWorkorderPage(workorderId) {
    let searchParams = new URLSearchParams();
    searchParams.set("account", modelObj.WaterDirectNavigation.SelectedUserAssignedAccount);
    searchParams.set("workorderId", workorderId);
    searchParams.set("backUrl", window.location.href);
    window.location = `/WaterDirect/DiEquipment/ViewWorkorder?${searchParams.toString()}`;
}

function GetOrderPage() {
    let searchParams = new URLSearchParams();
    searchParams.set("account", modelObj.WaterDirectNavigation.SelectedUserAssignedAccount);
    searchParams.set("backUrl", window.location.href);
    window.location = `/WaterDirect/DiEquipment/Order?${searchParams.toString()}`;
}

function SubmitDIOrder(e) {
    e.preventDefault();
    $('#btnOrderSubmit').attr('disabled', 'disabled');
    var url = $(this).attr("action");
    var formData = $(this).serialize();
    // var model = {
    //     IsSwap: $("input[type='radio'][name='SwapOptions']").val(),
    //     DeliveryOption: $("input[type='radio'][name='DeliveryOptions']").val(),
    //     DeliveryDate: $("#deliveryDatePicker").val(),
    //     PONumber: $("#txtPONumber").val(),
    //     SelectedDILoad: $("#selectDILoad").val(),
    //     TrailerQuantity: $("#spinnerTrailerQuantity").val(),
    //     OrderNotes: $("#txtOrderNotes").val(),
    //     WaterDirectNavigation: GetWaterDirectNavigation()
    // }
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "json",
        success: function (result) {
            console.log(result);
            // $("#divOrderHistory").html(result);
            // BindOrderHistoryEvents();
        },
        error: function (result) {
            console.log(result);
        },
        complete: function() {
            $('#btnOrderSubmit').removeAttr('disabled');
        }
    });
}