============================[********* ReadMe File: Znode Multifront 9.6.4.0 *********]================================

Updated April 23, 2021
---------------------------

CONTENTS OF README
---------------------
   
 1. Introduction
 2. Znode_Multifront_SourceCode_9.6.4.0.zip
    2.1. PowerBIReport
    2.2. ZnodeMultifront
         2.2.1. SetupFiles
         2.2.2. Database
         2.2.3. Projects
         2.2.4. SharedLibraries
         2.2.5. Media
    2.3. ZnodePayment
         2.3.1. Database
         2.3.2. PaymentApp
    2.4. ReadMe.txt



1. INTRODUCTION
---------------
The ReadMe file helps to understand the folder structure of Znode Multifront 9.6.4.0 and the manual setup procedure. 
It includes:
	- Information about required softwares.
	- Instructions to setup source code.
	- Instructions to upgrade the database.
	- Instructions to make the theme compatible.


2. Znode_Multifront_SourceCode_9.6.4.0.zip
------------------------------------------
Zip file contains the following folders:

2.1. PowerBIReport
	- It includes a sample ZnodeRealTimeDashboard.pbix file which is useful for configuring the PowerBI report in project.
2.2. ZnodeMultifront
	- It includes all the folders and files which are required to run the Znode Multifront.
2.3. ZnodePayment
	- It includes all the folders and files which are required to run the patch to the Znode Payment.
2.4. Readme.txt
	- It includes :
		- The guidelines and instructions on the folder structure of Znode Multifront 9.6.4.0
		- Steps to set up the Znode Multifront 9.6.4.0 source code in the development environment along with the database creation.
		- Steps to upgrade Znode Multifront from 9.0.x to 9.6.4.0


2.2. ZNODE MULTIFRONT
---------------------
ZnodeMultifront folder has the following folder structure:

2.2.1. SetupFiles
2.2.2. Database
2.2.3. Project
2.2.4. SharedLibraries
2.2.5. Media


2.2.1. SETUP FILES
------------------
The following prerequisite softwares are provided in the Setup Files:
   a. .NET FrameWork 4.8
   b. jdk-11.0.6_windows-x64_bin
   c. JAVA_HOME
   d. elasticsearch-7.6.0  
   e. mongodb-win32-x86_64-2012plus-4.2.3-signed
   f. setup.exe

Important:
a.) Prior to launching the Znode Multifront 9.6.4.0 from the source code, it is mandatory to install all prerequisite softwares provided in SetupFiles.
b.) Default session timeout has been changed to 120 and 60 for Znode admin and web store respectively.

=> a. .NET FrameWork 4.8
------------------------
--> To install .NET FrameWork, follow the below steps:

- For development environment, install .NET FrameWork 4.8 by following the below steps:
	Step 1. Run the ndp48-devpack-enu.exe to install.
	Step 2. Keep the default selection and continue with the installation.

- For hosted environment, install .NET FrameWork 4.8 by following the below steps:
	Step 1. Run the ndp48-web.exe to install.
	Step 2. Keep the default selection and continue with the installation.
	
=> b. jdk-11.0.6_windows-x64_bin
----------------------------
--> To install JDK follow the below steps:
	Step 1. Run the jdk-11.0.6_windows-x64_bin.exe to install.
	Step 2. Keep the default selection and continue with the installation.

=> c. JAVA_HOME
---------------
--> To set the Java Variable, follow the below steps:
	Step 1. Run the batch file JAVA_HOME.bat in administrative mode.
	Step 2. Press any key to continue.

=> d. elasticsearch-7.6.0 
-------------------------
--> To install Elasticsearch follow below steps:

- If an older version of Elasticsearch is installed already in the system then please uninstall it by following the below steps:
Step 1. Open Command Prompt in administrative mode and write in following command lines:
        Step 1.1. C:\elasticsearch-5.5.0\bin>service.bat stop
        Step 1.2. C:\elasticsearch-5.5.0\bin>service.bat remove

- Following are the steps to install and start the Elasticsearch:
Step 1. Open Command Prompt in administrative mode and write in following command lines:
        Step 1.1. C:\elasticsearch-7.6.0\bin>elasticsearch-service.bat install
        Step 1.2. C:\elasticsearch-7.6.0\bin>elasticsearch-service.bat start.
        Step 1.3. Go to services right click on Elasticsearch service click on Properties and set as Automatic.

Note: 
a) Make sure to provide the correct path of Elasticsearch in command prompt.
   a.1. If the Elasticsearch is installed on D: drive then a path needs to be provided accordingly.	
b) While migrating from Znode version 9.0.x to 9.6.4.0, delete the old Elasticsearch index by following the below steps:
   b.1. Stop the Elasticsearch service.
   b.2. Go to indices folder where the Elasticsearch is installed.
          E.g.: \elasticsearch-5.5.0\data\nodes\0\indices
   b.3. Delete the old index from this folder.
   b.4. Start the Elasticsearch service.
   b.5. Go to admin and create the new index for respective catalog.
	   
=> e. mongodb-win32-x86_64-2012plus-4.2.3-signed
----------------------------------------------------------

- For development environment, install MongoDB by following the below steps:
	Step 1. Run the "mongodb-win32-x86_64-2012plus-4.2.3-signed.exe" to install MongoDB.
	Step 2. In service configuration, enter the desired data and log directory.
	Step 3. Keep the default selection and continue with the installation.

- For hosted environment, install MongoDB by following the below steps:
	Step 1. Run the "mongodb-win32-x86_64-2012plus-4.2.3-signed.exe" to install MongoDB.
	Step 2. In service configuration, enter the desired data and log directory.
	Step 3. Keep the default selection and continue with the installation.
	Step 4. Open .\MongoDB\Server\4.2\bin\mongod.cfg file and change bindIP under network interfaces as follow:
			"bindIP: "127.0.0.1,w.z.y.z" where w.x.y.z is the ip address of the hosted environment.
	Step 5. Restart the "MongoDB server" service.

Note:
a.) While migrating from Znode version 9.0.x to 9.6.4.0, it is recommended to delete the old Mongo data and then publish it with the fresh data.
b.) Uninstall previous version of MongoDB from add or remove programs.

=> f. setup.exe
---------------
--> To configure Java Variable, follow the below steps:
	Step 1. Run the "setup.exe" to install.
	Step 2. Keep the default select and continue with the installation.


2.2.2. DATABASE
---------------

It includes the following three folders:

a. Znode Multifront 9.6.4.0 Database Script (for the fresh installation).
b. Znode Multifront Upgrade Scripts.
	- b.1. This is required if any previous version of Znode Multifront (9.0.0 and onwards) is installed.
c. Steps To Enable Mongo DB Authentication.
d. Delete Scripts (Znode Default or Old Data Delete).
	- d.1. This is optional.
	- d.2. If required, then follow below guidelines:
		-d.2.1. Execute the delete scripts in following sequence:
			- First, Delete All Products Script.sql.
			- Second, Delete All Category Script.sql.
			- Third, Delete All Catalog Script.sql.
			- Fourth, Delete All Orders Script.sql.
			Note: No sequence is required to execute delete order script.

	Note: Executing all the above delete scripts will delete the complete data from respective database tables.

--> a. Znode Multifront 9.6.4.0 Database Script (Fresh Installation)
       - To create Znode Multifront 9.6.4.0 fresh database, run the "Znode_Multifront_963.sql" script in SQL server management studio.	

For fresh installation, there are three choices for data scripts:
	 1. B2C Data Script(i.e. Data with FineFoods, Nut WholeSellers, etc. Catalogs)
	 2. B2B Data Script(i.e. Data with Maxwell's Hardware Catalog)
	 3. Znode Multifront Script Without Sample Data(i.e. The script with no default or sample data or any catalogs - Those who want to start with fresh catalog, categories, etc can take this script.)

- To create Znode Multifront 9.6.4.0 fresh database for B2C data, run the "Znode_Multifront_964.sql" script in SQL server management studio.

- To create Znode Multifront 9.6.4.0 fresh database for B2B data, run the "Znode_Multifront_964_B2B.sql" script in SQL server management studio.	

- To create Znode Multifront 9.6.4.0 without any Sample Data, run the "ZnodeMultifrontScriptWithoutSampleData.sql" script in SQL server management studio.	


--> b. Znode Multifront Upgrade Scripts
       - Refer this folder to upgrade Znode Multifront either from 9.0.1 to 9.0.2 or from 9.0.2 to 9.0.3 or from 9.0.3 to 9.0.4 or from 9.0.4 to 9.0.5.1 or from 9.0.5.1 to 9.0.6.0 or from 9.0.6 to 9.1.0.0 or 9.1.0.0 to 9.1.1.0 or 9.1.1.0 to 9.2.0.0 or 9.2.0.0 to 9.2.1.0 or 9.2.1.0 to 9.3.0.0 or 9.3.0.0 to 9.3.1.0 or 9.3.1.0 to 9.3.2.0 or 9.3.2.0 to 9.4.0.0 or 9.4.0.0 to 9.5.0.0 or 9.5.0.0 to 9.6.0.0 or 9.6.0.0 to 9.6.1.0 or 9.6.1.0 to 9.6.2.0 or 9.6.2.0 to 9.6.3.0 or 9.6.3.0 to 9.6.4.0
       - In this folder,  below files are provided:
		- RunUpgradeScript.bat
		- UpgradeScriptFrom901To902.sql
		- UpgradeScriptFrom902To903.sql
		- UpgradeScriptFrom903To904.sql
		- UpgradeScriptFrom904To9051.sql
		- UpgradeScriptFrom9051To906.sql
		- UpgradeScriptFrom906To910.sql
		- UpgradeScriptFrom910To911.sql
		- UpgradeScriptFrom911To920.sql
        - UpgradeScriptFrom920To921.sql
        - UpgradeScriptFrom921To930.sql
        - UpgradeScriptFrom930To931.sql
		- UpgradeScriptFrom931To932.sql
		- UpgradeScriptFrom932To940.sql
		- UpgradeScriptFrom940To950.sql
        - UpgradeScriptFrom950To960.sql
		- UpgradeScriptFrom960To961.sql
		- UpgradeScriptFrom961To962.sql
        - UpgradeScriptFrom962To963.sql
		- UpgradeScriptFrom963To964.sql

       - To upgrade the database please follow below instructions:

		Step 1. Execute Batch file "RunUpgradeScript.bat".
		Step 2. Follow the below instructions:
			Step 2.1. Enter SQL Server instance name.(example : TestServer\sql2016)
			Step 2.2. Enter User Name.(example : znodeuser)
			Step 2.3. Enter Password
			Step 2.4. Enter Database name (example : ZnodeMultifront963 which is already available)
		Step 3. Select the folder that contain the upgrade scripts (example: \\Znode Multifront 9.6.4.0\ZnodeMultifront\Database\Upgrade Scripts)
		Step 4. If required, save the log and press any key to continue on command prompt.
		Step 5. Once done, the command prompt will close automatically.

Important Note: It is recommended to use B2C data script for the projects which will be upgraded to 9.6.4 version, because there is no upgrade script available for the B2B data. The upgrade script for B2B data will be available in one of the upcoming releases.

--> c. Steps To Enable Mongo DB Authentication
       - To achieve this, refer text file "Mongo Authentication Steps.txt" under the "Steps To Enable Mongo DB Authentication" folder. 



2.2.3. PROJECTS
---------------

It includes one solution file and the following five folders:
a. Znode.Engine.Admin
b. Znode.Engine.API
c. Znode.Engine.Webstore
	c.1. Themes Folder
d. Libraries

Note: If any changes, customizations or new keys are added in the web.confi files of 9.0.x version of Znode Multifront then those should be added in the respective web.config files.

=> Follow below guidelines to launch the Znode Multifront 9.6.4.0:

--> a. Znode.Engine.Admin
       - Open Znode.Engine.Admin Folder
       - Open web.config and update the connection string with valid Database Credentials and Database name (Initial Catalog).

--> b. Znode.Engine.API
       - Open Znode.Engine.API folder.
       - Open web.config and update the connection string with valid Database Credentials and Database name (Initial Catalog).

--> c. Znode.Engine.Webstore
       - Open Znode.Engine.Webstore folder.
       - Open web.config and update the connection string with valid Database Credentials and Database name (Initial Catalog).

       --> c.1. Themes Folder
	- If using an older version of Znode Multifront (9.0.0 and onwards) then please follow the instructions provided in the knowledge base "(http://knowledgebase.znode.com)" to make the theme compatible with Znode Multifront 9.5.0.0.

--> d. Libraries 
	Note: Delete the Connected Services folder from the Znode.Engine.ERPConnector project as this one should not be a part of SDK.


2.2.4. SHARED LIBRARIES
-----------------------

It includes all the required DLLs/references for Znode source code.


2.2.5. MEDIA
------------------
There are two media folders available for B2B data and B2C data respectively. These folders will be available on the \ZnodeMultifront\Media path.
   -The projects which will use B2B data should extract the B2BMedia rar file on the path \ZnodeMultifront\Projects\Znode.Engine.API\Data and 
   -The projects which will use B2C data should extract the B2CMedia rar file on the path \ZnodeMultifront\Projects\Znode.Engine.API\Data.


2.3. ZNODE PAYMENT
------------------
	
It includes the following two folders:
1. Database
2. PaymentApp

2.3.1. DATABASE
---------------
a. Znode Multifront Payment 9.6.4.0 Database Script (Fresh Installation)
   - To create Znode Multifront Payment 9.6.4.0 fresh database, run the  "Znode_Multifront_964_Payment.sql" script in SQL server management studio.	

b. Upgrade Script
   - To upgrade Znode Multifront payment from 9.0.0 to 9.0.1, run the "Znode_multifront_Payment_90TO901.sql" script in SQL server management studio. 
   - To upgrade Znode Multifront payment from 9.0.1 or onwards to 9.0.5.1, run the "Znode_multifront_Payment_901TO9051.sql" script in SQL server management studio. 
   - To upgrade Znode Multifront payment from 9.0.5.1 or onwards to 9.0.6.0, run the "Znode_multifront_Payment_9051TO906.sql" script in SQL server management studio.
   - To upgrade Znode Multifront payment from 9.3.0.0 to 9.3.1.0, run the "Znode_multifront_Payment_930To931.sql" script in SQL server management studio.  
   - To upgrade Znode Multifront payment from 9.6.0.0 to 9.6.1.0, run the "Znode_multifront_Payment_960To961.sql" script in SQL server management studio.  
   - To upgrade Znode Multifront payment from 9.6.2.0 to 9.6.3.0, run the "Znode_multifront_Payment_962To963.sql" script in SQL server management studio.

2.3.2 PAYMENT APP
-----------------
 
It includes the following folder:		
  a. Znode.Multifront.PaymentApplication

Note: If any changes, customizations or new keys are added in the web.confi files of 9.0.x version (or above) of Znode Multifront then those should be added in the respective Payment web.config file.

=> Follow below guidelines to update the connections strings for the Znode Multifront Payment 9.6.4.0:
     - Go to Znode.Multifront.PaymentApplication.Api.
     - Open web.config and update the connection string with valid Database Credentials and Database name (Initial Catalog).

============================================================================================================================

Copyright 2021, Znode LLC, All Rights Reserved. www.znode.com